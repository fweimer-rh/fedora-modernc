* [Fix implicit int compiler warning in configure check for PTHREAD_SCOPE_SYSTEM](https://github.com/python/cpython/commit/12078e78f6e4a21f344e4eaff529e1ff3b97734f)
* https://bugzilla.redhat.com/show_bug.cgi?id=2147519
* https://src.fedoraproject.org/rpms/python3.8/c/e0317d05dce203076ca28b16aaf9ab1c66198930?branch=rawhide
