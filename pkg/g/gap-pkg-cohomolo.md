* [gap-pkg-cohomolo: Build in C89 mode due to C type errors](https://bugzilla.redhat.com/show_bug.cgi?id=2190297)
* [Lots of type safety errors](https://github.com/gap-packages/cohomolo/issues/36)
* https://src.fedoraproject.org/rpms/gap-pkg-cohomolo/c/cfd81ea6df452b2d3ef4b8eaa4128be6db6cc74e?branch=rawhide
