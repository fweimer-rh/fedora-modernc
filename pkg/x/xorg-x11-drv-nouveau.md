* [fb: Declare wfbFinishScreenInit, wfbScreenInit for !FB_ACCESS_WRAPPER](https://gitlab.freedesktop.org/xorg/xserver/-/merge_requests/1114)
* https://src.fedoraproject.org/rpms/xorg-x11-server/c/c2b6cc043d2760f168e97ce7860c344357645e25?branch=rawhide
* Note: Fix is in the xorg-x11-server package, but it benefits the xorg-x11-drv-nouveau package.