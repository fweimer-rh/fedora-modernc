* [fixed query buffer overrun and SIGPIPE in C API, mantis bug 489 \ 504](https://github.com/sphinxsearch/sphinx/commit/da41e452c5acc99ffe40f48be32d9ca7e55c407c)
* [api/libsphinxclient/test.c: Compiler compatibility fixes](https://github.com/sphinxsearch/sphinx/pull/47)
* https://src.fedoraproject.org/rpms/sphinx/c/3fb17b557bc85758e0af5ce89b199e045c253dc2?branch=rawhide
* https://src.fedoraproject.org/rpms/sphinx/c/0827c4c5202e77a2bcd914bf182b24d6f4248c49?branch=rawhide
