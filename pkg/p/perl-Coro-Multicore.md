* [perl-Coro-Multicore: Use of incompatible pointer type in perlmulticore.h](https://bugzilla.redhat.com/show_bug.cgi?id=2254339)
* https://src.fedoraproject.org/rpms/perl-Coro-Multicore/c/91469a7ae2a31dcfafa786a74d8d4aad4d3781e4?branch=rawhide (incompatible-pointer-types issue is in downstream-only patch)
