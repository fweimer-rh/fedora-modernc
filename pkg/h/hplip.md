* [Compatibility with strict(er) C99 compilers](https://bugs.launchpad.net/hplip/+bug/1997875)
* https://src.fedoraproject.org/rpms/hplip/c/bbb19dff5e32c47720b7523412dd7a59112a19a3?branch=rawhide
* https://src.fedoraproject.org/rpms/hplip/c/a3d30bea25d16c72d929bcc8f20877e9195c3c12?branch=rawhide (further tweaks by the package maintainer)
* [FTBFS with GCC 14 due missing incompatible pointer types](https://bugs.launchpad.net/hplip/+bug/2048780)
* https://src.fedoraproject.org/rpms/hplip/c/d726b77698ed806fb84e482fb40202548095fbe1?branch=rawhide
