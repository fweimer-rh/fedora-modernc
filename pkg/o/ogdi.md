* [contrib/gdal: Fix C99 compatibility issue](https://github.com/libogdi/ogdi/pull/25)
* https://src.fedoraproject.org/rpms/ogdi/c/06478be7a2579ca5160651dc6476ad7c68c4cf8f?branch=rawhide
