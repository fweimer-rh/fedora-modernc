* [build: Fix "4-digit year modifier" test](https://gitlab.gnome.org/GNOME/tracker/-/commit/f7393d61803815b19a1f210b197cce423ae3cc01)
* https://src.fedoraproject.org/rpms/tracker/c/4a85d69ff0405499650a2b52b766f73f2bce80ac?branch=rawhide
