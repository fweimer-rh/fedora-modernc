* [Remove obsolete AIM plugin](https://sourceforge.net/p/bitchx/git/ci/35b1a65f03a2ca2dde31c9dbd77968587b6027d3)
* [Include string.h in arcfour's MD5 implementation, since it uses memset().](https://sourceforge.net/p/bitchx/git/ci/b786c75e7a07add860792926ea352a5f254b67f7)
* [Possum's llist uses memcpy, so include string.h and eliminate a warning.](https://sourceforge.net/p/bitchx/git/ci/bc485f95a825c46ca7ce05274e37c7ba7dffb1ab)
* https://src.fedoraproject.org/rpms/BitchX/c/72270fdd777f75b5385f91f377188498b0d1f6b4?branch=rawhide
* [BitchX: Work around C99 compatibility issues](https://bugzilla.redhat.com/show_bug.cgi?id=2148940)
* https://src.fedoraproject.org/rpms/BitchX/c/451c1eb22b728cb98027e60012a02bab49dea18a?branch=rawhide
