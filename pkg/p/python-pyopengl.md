* [accelerate: Fix C type errors for GCC 14/Clang compatibility](https://github.com/mcfletch/pyopengl/pull/112)
* https://src.fedoraproject.org/rpms/python-pyopengl/c/e71bd3bda3229ab4a46c3a78f4552bd521f0eb39?branch=rawhide
