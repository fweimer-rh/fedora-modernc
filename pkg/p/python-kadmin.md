* [Python 3 no longer uses tp_print in PyTypeObject](https://github.com/rjancewicz/python-kadmin/pull/72)
* https://src.fedoraproject.org/rpms/python-kadmin/c/dd18c14f2771578d1e39181149e56da10231c38b?branch=rawhide
