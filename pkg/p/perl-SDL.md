* [perl-SDL will fail to build with GCC 14 because of an implicit _calc_offset() declaration](https://bugzilla.redhat.com/show_bug.cgi?id=2177189)
* [Fix implicit declaration of _calc_offset](https://github.com/PerlGameDev/SDL/pull/299)
* https://src.fedoraproject.org/rpms/perl-SDL/c/b6b5e69cb28b69ba8f0c2c0554a3ec3d60c45421?branch=rawhide
