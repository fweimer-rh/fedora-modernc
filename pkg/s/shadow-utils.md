* [Fix HAVE_SHADOWGRP configure check](https://github.com/shadow-maint/shadow/pull/595)
* https://src.fedoraproject.org/rpms/shadow-utils/c/bb5dc8ec04d089507c26f58974b056ae1aa334ca?branch=rawhide
