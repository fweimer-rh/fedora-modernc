* [Issue #5: incompatible function pointer passed in _XawImRealize()](https://gitlab.freedesktop.org/xorg/lib/libxaw3d/-/commit/34b1fb5d114b6c763c06dd9618c3d8ca11662e34)
* [Multisink.c: Clear -Werror=incompatible-pointer-types error](https://gitlab.freedesktop.org/xorg/lib/libxaw3d/-/commit/7b4af57e8a1471747e17fff184032d8250e598b6)
* https://src.fedoraproject.org/rpms/Xaw3d/c/f82090de9f5cb5e56d42bfd12f0eb86835f74923?branch=rawhide
