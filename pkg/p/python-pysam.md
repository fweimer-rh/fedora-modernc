* [configure.ac: Define _DEFAULT_SOURCE along with _XOPEN_SOURCE](https://github.com/samtools/htslib/pull/1711)
* [htslib: Fix mmap configure check for current compilers](https://github.com/pysam-developers/pysam/pull/1250)
* https://src.fedoraproject.org/rpms/python-pysam/c/18e228e80b2314f4b0c2b3d4c1eb3d211edc2939?branch=rawhide
