* [Implicit int declarations are incompatible with C99](https://sourceforge.net/p/tix/bugs/112/)
* https://src.fedoraproject.org/rpms/tix/c/c35b75cfd57e7ea5c480e9321a89726fb187b8e1?branch=rawhide
* [Incompatible pointer types, const correctness (GCC 14/Clang 16 compilation fixes)](https://sourceforge.net/p/tix/bugs/113/)
* https://src.fedoraproject.org/rpms/tix/c/0721d386bc0fd689121fff23bcf2f917b5d44e91?branch=rawhide
