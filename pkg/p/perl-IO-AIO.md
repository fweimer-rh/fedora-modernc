* [Patch for C compatibility issue (incompatible-pointer-types)](https://rt.cpan.org/Ticket/Display.html?id=150766)
* https://src.fedoraproject.org/rpms/perl-IO-AIO/c/e61f1757cd2d75223e1787c9f6dc8d6d8cea26ae?branch=rawhide
