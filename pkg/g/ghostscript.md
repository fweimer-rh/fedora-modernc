* [Bug 707130: Cast to void ** to avoid compiler warning](https://git.ghostscript.com/?p=ghostpdl.git;a=commitdiff;h=b7beb19ad06e08b889a44694ff813ed5f6c96da4)
* https://src.fedoraproject.org/rpms/ghostscript/c/56b3f24430a3613c5dc01c604861d71df5b9e6ae?branch=rawhide
