* [Fix fedora rawhide i686 compilation](https://github.com/syslog-ng/syslog-ng/pull/4816)
* [secure-logging: Changed local variable type as g_io_channel_write_chars as it's 4th parameter accepts gsize.](https://github.com/syslog-ng/syslog-ng/commit/ac13999810bced8f623b0e027579a95f23dc0641)
* https://src.fedoraproject.org/rpms/syslog-ng/c/e752074dc706595b928253ef9db44e062ae15814?branch=rawhide
