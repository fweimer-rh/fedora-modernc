* [config: Fix -Wimplicit-function-declaration](https://git.netfilter.org/conntrack-tools/commit/?id=6ce497caac85f53a54e359ca57ad0f9dc379021f)
* https://src.fedoraproject.org/rpms/conntrack-tools/c/bdc967187bab94c802285d99d398a6e3b776f989?branch=rawhide
* [read_config_yy: correct arguments passed to `inet_aton](https://git.netfilter.org/conntrack-tools/commit/?id=d417ceaa947c5f7f5d691037d0abe1deca957313)
* https://src.fedoraproject.org/rpms/conntrack-tools/c/6831489ff26936ffb3c3d23d538f42cdd4bc4dee?branch=rawhide
