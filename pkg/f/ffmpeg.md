* [avformat/ffrtmpcrypt: Fix int-conversion warning](https://git.ffmpeg.org/gitweb/ffmpeg.git/commitdiff/42982b5a5d461530a792e69b3e8abdd9d6d67052)
* https://src.fedoraproject.org/rpms/ffmpeg/c/b05e4ff5576598302a21fdd9c0c9b7c6e526f352?branch=rawhide
