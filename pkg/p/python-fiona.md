* [Fix leak in set_proj_search_path](https://github.com/Toblerity/Fiona/pull/1314)
* [Fix compiler warnings from extensions](https://github.com/Toblerity/Fiona/pull/1315)
* https://src.fedoraproject.org/rpms/python-fiona/c/01c63ea198a50fb203c8ab9bda4885b8806ec28b?branch=rawhide
* https://src.fedoraproject.org/rpms/python-fiona/c/7741031664dd839ead505307862c7166c80c5936?branch=rawhide
