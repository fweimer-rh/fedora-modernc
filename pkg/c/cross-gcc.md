* [libsanitizer: Avoid implicit function declaration in configure test](https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=7d7146102365f708a37401c902fce2f4024b546a)
* [libiberty: Fix C89-isms in configure tests](https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=63f3eae53683e857818c4bd3d1de719e1310e22a)
* https://src.fedoraproject.org/rpms/cross-gcc/c/0e195162fbe690a5a3c49fe55756b26c9d181d0d?branch=rawhide
