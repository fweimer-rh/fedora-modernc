* [glfs-truncate: Add missing int return types](https://github.com/gluster/glusterfs-coreutils/pull/38)
* https://src.fedoraproject.org/rpms/glusterfs-coreutils/c/2ccd69a80b0084b64f14c02e8637056184bde219?branch=rawhide
* [Fix error checking for glfs_creat](https://github.com/gluster/glusterfs-coreutils/pull/39)
* https://src.fedoraproject.org/rpms/glusterfs-coreutils/c/e799eadc950e1fdaefede02624bdf4033802b523?branch=rawhide
