* [Fix build warnings for aarch64-linux targets](https://github.com/libunwind/libunwind/commit/9ab9547fd696f78c228e0383886cf2fa274de987)
* https://src.fedoraproject.org/rpms/libunwind/c/bbfe2c4352c2e806c6498d488a6fe85e4a950c1c?branch=rawhide
