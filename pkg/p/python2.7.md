* [Fix implicit int compiler warning in configure check for PTHREAD_SCOPE_SYSTEM (#99085)](https://github.com/python/cpython/commit/12078e78f6e4a21f344e4eaff529e1ff3b97734f)
* [bpo-42598: Fix implicit function declarations in configure
 (GH-23690)](https://github.com/python/cpython/commit/674fa0a740151e0416c9383f127b16014e805990)
* [bpo-13153: Use OS native encoding for converting between Python and Tcl. (GH
-16545)](https://github.com/python/cpython/commit/06cb94bc8419b9a24df6b0d724fcd8e40c6971d6)
* https://src.fedoraproject.org/rpms/python2.7/c/537affdd3082a0b92b6632b000162750d0fd6de5?branch=rawhide
