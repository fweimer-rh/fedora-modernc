* [python-setuptools: Unclear how CCompiler.has_function works for functions with parameters](https://bugzilla.redhat.com/show_bug.cgi?id=2153037)
* [distutils.ccompiler: Make has_function work with more C99 compilers](https://github.com/pypa/distutils/pull/195)
* [Unclear how CCompiler.has_function works for functions with parameters](https://github.com/pypa/setuptools/issues/3648)
* https://setuptools.pypa.io/en/latest/history.html#v67-2-0
* https://src.fedoraproject.org/rpms/python-setuptools/c/8ae9b2a777c95ffe29388e5ee92bc0eac1a0f734?branch=rawhide
* Impacts packages other packages.
