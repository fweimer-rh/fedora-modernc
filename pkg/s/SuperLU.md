* [C99 compatibility fixes](https://github.com/xiaoyeli/superlu/pull/76)
* [Add more prototypes for BLAS functions](https://github.com/xiaoyeli/superlu/commit/891ce290edaf4e612180b06e1648b10dd4ec498c)
* [Include "slu_sdefs.h" instead of "slu_Cnames.h"](https://github.com/xiaoyeli/superlu/commit/007db2fb5fcb0082c9679fab7b36ef6133fa087b)
* [Explicitly declare main as returning int.](https://github.com/xiaoyeli/superlu/commit/525b8ec3a5ebf551b534471c2fb9bdf7a6eb5b9b)
* [[example] main() should actually return an int](https://github.com/xiaoyeli/superlu/commit/5766fa4f900353fe2d13fb09f40083f9c525a24c)
* [[testing] Include getopt.h](https://github.com/xiaoyeli/superlu/commit/5d6b443c1687d7ef842297fe064e14553e638fe5)
* https://src.fedoraproject.org/rpms/SuperLU/c/aaef16b14cf9dc0b28f55f9537f0557ea042f245?branch=rawhide
