* [m4/fp_leading_underscore.m4: Avoid implicit exit function declaration](https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9394)
* https://src.fedoraproject.org/rpms/ghc9.2/c/9d1a4b9d01af9a3a5427f516d8aea83694610464?branch=rawhide

