* [Update dlna-renderer plugin. Issue #94](https://github.com/pragha-music-player/pragha/commit/f5440a577c1041cf25d0bb6394b59e149f8f095f)
* [D'Oh!. Fix missing function due typo...](https://github.com/pragha-music-player/pragha/commit/b6c31e0c77a74458fcfa7938a0fbc3b55c7476fc)
* https://src.fedoraproject.org/rpms/pragha/c/e09cea4a556cebc057f199366e4fd8b72c25e0e1?branch=rawhide
