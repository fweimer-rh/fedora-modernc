* https://sourceforge.net/p/gmerlin/code/5451/ (adds `#include <string.h>` to `src/fill_test.c`)
* https://sourceforge.net/p/gmerlin/code/6246/ (removes implicit int in `clock_gettime` configure test)
* https://src.fedoraproject.org/rpms/gavl/c/66b035e0011b5ec6fada24f4f110ca28efac6360?branch=rawhide
