* [C99 compatibility fixes (implicit ints, implicit function declarations)](https://sourceforge.net/p/bristol/patches/3/)
* https://src.fedoraproject.org/rpms/bristol/c/33eae3760da6e69b619fa08e3476bcfdccdfa28f?branch=rawhide
