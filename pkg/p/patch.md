* [fchownat: Fix compilation error on Android 4.3.](https://git.savannah.gnu.org/cgit/gnulib.git/commit/?id=3c136a2cc38d71083f123231a8b9ad4b01930789) (also happens to fix an implicit declaration of `mkdir`)
* https://src.fedoraproject.org/rpms/patch/c/216dfdf4fffe533042a55371c11c97115a917566?branch=rawhide
