* [C-Kermit 304(dev07) 2013/11/24 Unix VMS](https://github.com/KermitProject/ckermit/commit/ca6e39fcf660827fc3525d58522d4786c16cf1c6)
  added `#include <time.h>` to `cmcmai.c`.
* [C-Kermit 305(alpha02) 2020/09/19 Unix VMS](https://github.com/KermitProject/ckermit/commit/9ada69d8f89896ad60f875795bebf7924de9ee9d) fixes
  *about 30 compile-time warnings that were reported by gcc 9.3.0
  on Ubuntu 20.04.1*.
* [C-Kermit 305(alpha06) 2021/12/17 Unix VMS](https://github.com/KermitProject/ckermit/commit/ef67f40581dd4b24457d53b8155b18b9c2b35b4e) fixes
  *numerous picky-compiler warnings*.
* https://src.fedoraproject.org/rpms/ckermit/c/19e64cef5f4eaebc6f1b08fff0e94f1e6708e987?branch=rawhide
