* [configure: Avoid implicit declaration of exit, strcmp in snprintf probes](https://github.com/xroche/httrack/pull/252)
* https://src.fedoraproject.org/rpms/httrack/c/e76eecd6154e50e1670577dbe3eb667b885d9010?branch=rawhide
