* https://bugzilla.redhat.com/show_bug.cgi?id=2186773
* [Fixed bug #1602 (Eliminates 6 warnings "implicit declaration of function" in some modules).](https://sourceforge.net/p/motif/code/ci/56dae22f7c2e7446852af1d68116f1809edae633)
* [Fixed bug #1605 (Eliminates 14 warnings of some types in WML tool).](https://sourceforge.net/p/motif/code/ci/d094e8c4a6a1890520ade3e1900229454dc2eac2)
* [Fixed bug #1609 (Eliminates 18 warnings of "incompatible implicit declaration" in modules of "clients" catalog).](https://sourceforge.net/p/motif/code/ci/7a52b1ea7e1ec88ba854db30a51d9f68f2f0078f)
* https://src.fedoraproject.org/rpms/motif/c/5e2c9f6f8b9516563500c6ea771ec2b852692b61?branch=rawhide
* [Fixed bug #1599 (Eliminates 14 warnings of incompatible type argument from s
ome modules).](https://sourceforge.net/p/motif/code/ci/23d12d1f25b33773018bb873c21547b0f4aab14c)
* https://src.fedoraproject.org/rpms/motif/c/b2f5f2f3d2d2694ff3426ed75cd752d3a5fb581b?branch=rawhide
