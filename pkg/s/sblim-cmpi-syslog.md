* [sblim-cmpi-syslog C99 compatibility fix](https://sourceforge.net/p/sblim/bugs/2770/)
* https://src.fedoraproject.org/rpms/sblim-cmpi-syslog/c/54f05577f1a2961db2ab0fb0610cb48b6d903368?branch=rawhide
