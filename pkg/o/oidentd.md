* [configure.ac: Use global instead of main fragment in __attribute__ checks](https://github.com/janikrabe/oidentd/pull/43)
* https://src.fedoraproject.org/rpms/oidentd/c/8dc0bd5a42556ee32943989018a55f7b416ef048?branch=rawhide
