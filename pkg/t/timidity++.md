* [Patch configure for C99 porting effort](https://src.fedoraproject.org/rpms/timidity++/c/1b55acbf2a7354055a100cf32a45832b91af7eea?branch=rawhide)
* [Fedora bug](https://bugzilla.redhat.com/show_bug.cgi?id=2165930)
* Patch sent to timidity-devel@lists.sourceforge.net but either it bounced or is held for moderation. Upstream not very active.
