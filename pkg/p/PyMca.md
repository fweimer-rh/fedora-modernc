* [Avoid implicit int types in PyMca5/PyMcaIO/sps/Src/sps_lut.c](https://github.com/vasole/pymca/pull/932)
* https://src.fedoraproject.org/rpms/PyMca/c/1b12e38502291c35350052a8cfb4539ad8b62f57?branch=rawhide
