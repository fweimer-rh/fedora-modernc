* [Fix C99 compatibility issue](https://sourceforge.net/p/sdlpango/patches/2/)
* https://sources.debian.org/src/guix/1.4.0-1/gnu/packages/patches/sdl-pango-header-guard.patch/
* https://src.fedoraproject.org/rpms/SDL_Pango/c/641385fb401b95836f01277b6476aaebbb0c5188?branch=rawhide
