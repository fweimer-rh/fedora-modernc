* [Antlr 4 feature file parser and CMake build system](https://github.com/adobe-type-tools/afdko/commit/26761e9616f8a52aa14e7041ac9cbb0e139a1d83)
* [Fix build failures discovered by an upcoming gcc-14 release](https://github.com/adobe-type-tools/afdko/pull/1730)
* https://src.fedoraproject.org/rpms/adobe-afdko/c/25087f03f500bf84a561571a1fd922cead793341?branch=rawhide
