* [tests: Too many arguments in call to 'create_menu'](https://gitlab.gnome.org/GNOME/gtk/-/commit/bcfc53066ae54d0adbf282d5678d9a4a7083b4d3)
* https://src.fedoraproject.org/rpms/gtk2/c/595aab443e7fefb1b4347c7959f82133ffe48c78?branch=rawhide
* [immodule: fix a GCC warning](https://gitlab.gnome.org/GNOME/gtk/-/commit/018a4255624928fb7d951f1d1c89196fe77e8267)
* [GtkScale: Fix late setting of marks](https://gitlab.gnome.org/GNOME/gtk/-/commit/345d865ac6b2f7760503ab144dd68e6062438b80)
* [testsuite/gtk/defaultvalue: Actually build...& fix](https://gitlab.gnome.org/GNOME/gtk/-/commit/ca94ff10ce2ea57bfde7692aaa558b1260c9ff75)
* [Fix fallout from g_object_ref change](https://gitlab.gnome.org/GNOME/gtk/-/commit/072f06abf713d7536ce2a84a5ac266f585c88d39)
* Note: `gtk2-c89-4.patch` and `gtk2-c89-6.patch` are custom patches that fix files that were removed upstream when they still had compatibility issues in them.
* https://src.fedoraproject.org/rpms/gtk2/c/a3a398f4f95684dba7a461acf389626eb2a3b90e?branch=rawhide
