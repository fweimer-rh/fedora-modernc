* https://src.fedoraproject.org/rpms/netpbm/c/fa99cfecf1a00e44206549775231bbd90ccf9126?branch=rawhide
* [netpbm: Broken pmjas_image_decode implementation results in GCC 14 error](https://bugzilla.redhat.com/show_bug.cgi?id=2259448)
* [netpbm: Out of bounds stack write in netpbm-cmuwtopbm.patch](https://bugzilla.redhat.com/show_bug.cgi?id=2259450)
* https://src.fedoraproject.org/rpms/netpbm/c/2b6ee3cbff09e2ca95310fd5b2c898e94c1f30d3?branch=rawhide
