* [HTML::Strip: Improve C99 compatibility with utf8_char_width prototype](https://rt.cpan.org/Public/Bug/Display.html?id=146734)
* https://src.fedoraproject.org/rpms/perl-HTML-Strip/c/5b7890f8047be09dfcdc38f55f3f7dee9421fbbf?branch=rawhide
