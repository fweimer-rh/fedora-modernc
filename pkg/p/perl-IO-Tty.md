* [Make function checks more robust within shared libs](https://github.com/toddr/IO-Tty/commit/1735a78561dbe139fd138caef2d44d81f5494fe7)
* https://src.fedoraproject.org/rpms/perl-IO-Tty/c/acddabbd47565a4e36210117eb8a3c11656907ea?branch=rawhide
