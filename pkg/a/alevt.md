* [Avoid implicit function declarations in lang.c](https://gitlab.com/alevt/alevt/-/merge_requests/10)
* https://src.fedoraproject.org/rpms/alevt/c/ab5940c9cf6e04eba4a4059c3bb5bb9807e9f0f8?branch=rawhide
