* [12.33: Fix implicit-values test](https://sourceware.org/git/?p=annobin.git;a=commitdiff;h=b90df262e1e2ea123688a9818c81a3adca9ece12#patch5)
* https://src.fedoraproject.org/rpms/annobin/c/01a58912b3894da10ce9cb62f0fd2e418fb69f1e?branch=rawhide
