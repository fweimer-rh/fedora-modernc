* [configure: Fix an "implicit int" warning #212](https://github.com/rkd77/elinks/pull/221)
* [Merge pull request #212 from submachine/master](https://github.com/rkd77/elinks/commit/d9d5633c0b8f8f300d3844980c08372bafad71e1)
* https://src.fedoraproject.org/rpms/elinks/c/79fd552452869904ef51390705b28f78ab035705?branch=rawhide
