* [cmake: Fix C compatibility of libunwind probes](https://invent.kde.org/sdk/heaptrack/-/merge_requests/24)
* https://src.fedoraproject.org/rpms/heaptrack/c/ed374ab2d834a7baa0bd0404aa2a2769b289d620?branch=rawhide
