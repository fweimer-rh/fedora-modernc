* [Implicit declaration of readlink due to _POSIX_*_SOURCE macro changes (C99 compatibility)](https://sourceforge.net/p/mcpp/patches/7/)
* https://src.fedoraproject.org/rpms/mcpp/c/0fb8df33b2c94274c02678d21bc1ceb8b9aae72b?branch=rawhide
