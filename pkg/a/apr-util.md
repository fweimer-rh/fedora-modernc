* [Avoid implicit ints in the configure script](https://bz.apache.org/bugzilla/show_bug.cgi?id=66396)
* https://src.fedoraproject.org/rpms/apr-util/c/4883e291377d6ae90b5be662e8a6b80f0e140576?branch=rawhide
