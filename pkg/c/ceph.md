* [GCC 14 compatibility fixes](https://github.com/ceph/ceph/pull/54974)
* https://src.fedoraproject.org/rpms/ceph/c/570bcf896eb5ed8fca9d45e78c1e36d0f1c1bebe?branch=rawhide
