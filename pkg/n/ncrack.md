* [Fix C99 compatibility issue](https://github.com/nmap/ncrack/pull/127)
* https://src.fedoraproject.org/rpms/ncrack/c/425a54633e220b6bafca37554e5585e2c6b48082?branch=rawhide
