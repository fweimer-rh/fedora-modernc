* [xwayland: Use correct pointer types on i386](https://gitlab.freedesktop.org/xorg/xserver/-/merge_requests/1255)
* https://src.fedoraproject.org/rpms/xorg-x11-server-Xwayland/c/a331324d7cf03fc2df22156e3674514d5c0c7170?branch=rawhide
