* [BUG: F_INT type conflict with f2py translation of INTEGER type on i386](https://github.com/scipy/scipy/issues/19993)
* [scipy fails to build with GCC 14 on i686: error: assignment to ‘npy_int32 *’ {aka ‘long int *’} from incompatible pointer type ‘int *’ [-Wincompatible-pointer-types]](https://bugzilla.redhat.com/show_bug.cgi?id=2258823)
* [Disable incompatible-pointer-types errors on i686 (#2258823)](https://src.fedoraproject.org/rpms/scipy/c/eb436d61e9f027cb9c61d1995629d9f0c9bd1b68?branch=rawhide)
