* [codegen: Emit GCC diagnostics pragmata for GCC 14 compatibility](https://gitlab.gnome.org/GNOME/vala/-/merge_requests/369)
* [Downgrade GCC 14 C type errors to warnings](https://src.fedoraproject.org/rpms/vala/c/6c9c11e678f5c693c437fa73ccb5cb0c2d33fc84?branch=rawhide)
* [Downgrade GCC 14 -Wint-conversion errors to warnings](https://src.fedoraproject.org/rpms/vala/c/67fbbfc5e7b859ad8081c8332f982c83ceec922a?branch=rawhide)
