* [Add "lexparse.h" headers for lexer/parser integration](https://github.com/italiangrid/voms/pull/112)
* https://bugzilla.redhat.com/show_bug.cgi?id=2168585
* https://src.fedoraproject.org/rpms/voms/c/fd212d2e4346a284b861a66b672eb4d44cd3c4a2?branch=rawhide
