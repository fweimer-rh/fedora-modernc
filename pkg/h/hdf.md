* [hdf: Update from upstream Git to address C99 compatibility issues](https://bugzilla.redhat.com/show_bug.cgi?id=2167466)
* [Set build_type_safety_c to 0 (#2167466)](https://src.fedoraproject.org/rpms/hdf/c/e557ba533f94d39743d19b693a48288ece2e98ff?branch=rawhide)
