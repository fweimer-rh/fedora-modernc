* [Forward declarations missing for rename_area and remove_area](https://sourceforge.net/p/slashem/bugs/963/)
* [Additional C99 compatibility fixes.](https://sourceforge.net/p/slashem/bugs/964/)
* https://src.fedoraproject.org/rpms/slashem/c/27e19eb934e010d414cd42811b62f132c748a021?branch=rawhide
* https://src.fedoraproject.org/rpms/slashem/c/aea34ff0674452694fecf6569539fde5b6eb9efa?branch=rawhide

