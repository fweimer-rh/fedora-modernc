* [configure: fix test code to match gethostbyname_r prototype. (#75)](https://github.com/asterisk/asterisk/commit/0e6295128c605f1cf41d897c53e6e23e893a6ffa)
* https://src.fedoraproject.org/rpms/asterisk/c/1aa3344d46664bac64ef2389762f6d3ebc0daa13?branch=rawhide
