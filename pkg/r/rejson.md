* [Fix implicit declaration of 'strncasecmp'](https://github.com/goodform/rejson/pull/1)
* https://src.fedoraproject.org/rpms/rejson/c/ea57908093bf367a8178b6a7db56ee3b10ded8f7?branch=rawhide

