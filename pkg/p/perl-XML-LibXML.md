* [libxml-mm: Fix function prototypes in function pointers](https://github.com/shlomif/perl-XML-LibXML/commit/8751785951fbde48ffa16a476da3e4adb2bbcde5)
* https://src.fedoraproject.org/rpms/perl-XML-LibXML/c/f5d43a784c86964e057c2d90de8ea98ec6a293ec?branch=rawhide
