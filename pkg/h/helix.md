* [Do not disable GNU extensions when building grammars](https://github.com/helix-editor/helix/pull/8953)
* [Fix error when building with Clang](https://github.com/jaredramirez/tree-sitter-rescript/commit/d9fc339805666b25482823cfad46c3c62074a4ea)
* https://src.fedoraproject.org/rpms/helix/c/47b402d7fd55b47d40b7e21b8d910cb1bf5e0789?branch=rawhide
