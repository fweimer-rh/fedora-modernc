* [Bug #150404 for Data-Pond: [PATCH] use uvchr_to_utf8_flags instead of uvuni_to_utf8_flags (which is removed in perl 5.38.0)](https://rt.cpan.org/Public/Bug/Display.html?id=150404)
* https://src.fedoraproject.org/rpms/perl-Data-Pond/c/b4cbf1320650db87609b025aafa762845bf50672?branch=rawhide
