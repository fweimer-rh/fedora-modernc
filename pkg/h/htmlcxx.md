* [htmlcxx: Build in C89 mode due to undeclared functions in lexer/parser integration](https://bugzilla.redhat.com/show_bug.cgi?id=2168170)
* [Update css_syntax.y for use with less ancient Bison](https://sourceforge.net/p/htmlcxx/code/ci/807081583ea58b07a5ff2e0659f4173492befb8a/) (not yet in the Fedora version)
* https://src.fedoraproject.org/rpms/htmlcxx/c/5b0ca2e5980a2b23609bceb3adef3a72f7bfe280?branch=rawhide
