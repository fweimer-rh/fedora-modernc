* [Avoid C89-only constructs in Configure](https://github.com/lsof-org/lsof/pull/265)
* [Upstream commit](https://github.com/lsof-org/lsof/commit/cc24afbe865af847374f6155adefbad8cdab3b17)
* https://src.fedoraproject.org/rpms/lsof/c/9b1730c7b0f3b7e658e0578c863d60bea430d938?branch=rawhide
