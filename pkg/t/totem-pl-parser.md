* [Return FALSE instead of NULL, to match the gboolean return type](https://gitlab.gnome.org/GNOME/totem-pl-parser/-/merge_requests/53)
* https://src.fedoraproject.org/rpms/totem-pl-parser/c/8aea802762fc51d0b0ff0e446083309de17e68f9?branch=rawhide
