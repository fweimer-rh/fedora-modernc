* [fails to compile with clang 16](https://sourceforge.net/p/ruamel-yaml-clib/tickets/22/)
* p[ruamel-yaml] ReaderError: unacceptable character on s390x architecture](https://bugzilla.redhat.com/show_bug.cgi?id=2042422) adds downstream-only `fix-typecasts-s390x.patch`.
* https://src.fedoraproject.org/rpms/python-ruamel-yaml-clib/c/57b88ff82b4446c5d759b703481fee976cabe123?branch=rawhide updates `fix-typecasts-s390x.patch`.
