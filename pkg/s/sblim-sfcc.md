* [sblim-sfcc patch for C99 compatibility](https://sourceforge.net/p/sblim/bugs/2767/)
* https://src.fedoraproject.org/rpms/sblim-sfcc/c/e77b4004104da30649eaf7dc7912044e0295939d?branch=rawhide
* [sblim-sfcc: Pointer casts for compatibility with C89, GCC 14](https://sourceforge.net/p/sblim/bugs/2771/)
* https://src.fedoraproject.org/rpms/sblim-sfcc/c/c410dd827bbbd2fb12ad1428f86a89f1bded07a4?branch=rawhide
