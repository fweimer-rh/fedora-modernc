* [SWIG_Python_str_AsChar removal in SWIG 4.2.0](https://github.com/NLnetLabs/ldns/pull/232)
* [32-bit compatibility for Python SWIG bindings](https://github.com/NLnetLabs/ldns/pull/233)
* https://src.fedoraproject.org/rpms/ldns/c/15dbd0729dcc0b9ec083def611e11472bc4caff1?branch=rawhide
