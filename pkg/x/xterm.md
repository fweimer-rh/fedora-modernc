* [xterm: XkbBell configure check uses wrong argument type](https://bugzilla.redhat.com/show_bug.cgi?id=2251945)
* https://src.fedoraproject.org/rpms/xterm/c/0d7792d8a5476036ff472ae3b8de5b1dc85de365?branch=rawhide
