* [[PATCH] configure: Remove obsolete check for -b i486-linuxaout](https://lists.gnu.org/archive/html/emacs-devel/2022-12/msg01019.html)
* [[PATCH] configure: Include <stdlib.h> for malloc in alternate signal stack test](https://lists.gnu.org/archive/html/emacs-devel/2022-12/msg01020.html)
* https://src.fedoraproject.org/rpms/emacs/c/c079ae3eb0e518954e990482a9e2c4ca588160ab?branch=rawhide
