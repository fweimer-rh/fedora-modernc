* [Fix libsubid detection](https://github.com/containers/skopeo/pull/2190)
* https://src.fedoraproject.org/rpms/skopeo/c/71babc57960af915b4917befcbf7053d3174753a?branch=rawhide
