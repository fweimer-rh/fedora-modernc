* [Fix C type errors in audio.c](https://github.com/g0orx/linhpsdr/pull/127)
* https://src.fedoraproject.org/rpms/linhpsdr/c/e3ba94a3be5590714b6fbd5f8113cdf7a25ee175?branch=rawhide
