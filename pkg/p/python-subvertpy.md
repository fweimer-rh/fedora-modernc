* [Python 3 does not have tp_print in PyTypeObject](https://github.com/jelmer/subvertpy/pull/48)
* https://src.fedoraproject.org/rpms/python-subvertpy/c/8edc9be5c9f18663474d21bb63df79b0ce8e70d1?branch=rawhide
