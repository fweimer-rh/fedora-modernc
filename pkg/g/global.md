* [[Bug fixed] Configure failed (realpath not found) on MacOS 11.1.](http://cvs.savannah.gnu.org/viewvc/global/global/configure.ac?r1=1.204&r2=1.205)
* [Added 'int' before the main function.](http://cvs.savannah.gnu.org/viewvc/global/global/configure.ac?revision=1.211&view=markup)
* [Re: error: POSIX.1-2008 realpath(3) is required on macos](https://lists.gnu.org/archive/html/bug-global/2022-10/msg00000.html)
* https://src.fedoraproject.org/rpms/global/c/238b1020a4c02c178de71fecbe52ffccfdfeaed4?branch=rawhide
