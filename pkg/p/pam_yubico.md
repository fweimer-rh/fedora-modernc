* [Fix implicit declaration warning of mkostemp](https://github.com/Yubico/yubico-pam/commit/be2fdfdada6d419b4b80de2449e845d323516f72)
* https://src.fedoraproject.org/rpms/pam_yubico/c/21d61834be7458db67e500ffe3be471ebba51a67?branch=rawhide
