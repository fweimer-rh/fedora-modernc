* [build: Correct incompatible type warning from check for I_CAL_EMAIL_PARAMETER](https://gitlab.gnome.org/GNOME/evolution-data-server/-/commit/55558d3c23e68aada59c5deb59a664aea263f075)
* https://src.fedoraproject.org/rpms/evolution-data-server/c/68be1c4af7dd3a71aeb94ece2b4f50dfa803a2c4?branch=rawhide
