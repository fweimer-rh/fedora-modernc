* [string to double for modern units uses atof (#55)](https://github.com/BlueBrain/mod2c/commit/450cb0e8b912c935f31a78b25a7f7ecc70975bdb)
* [Avoid error with new clang/flex : implicit declaration of function 'isatty' (#60)](https://github.com/BlueBrain/mod2c/commit/e02365e61dece84efed8a63d4b550c3ca690d65c)
* https://src.fedoraproject.org/rpms/mod2c/c/b410c44303d219790d7f472537f170c188a6f3b7?branch=rawhide
