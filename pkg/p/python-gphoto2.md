* [Use PyObject_GC_UnTrack in lieu of the old variant](https://github.com/swig/swig/commit/db9822788e183b79457d6bedab8b9a5cabb4cd5e)
* https://src.fedoraproject.org/rpms/python-gphoto2/c/3a11b6b2cde76842d268a0f730aede3ba9f247a7?branch=rawhide

