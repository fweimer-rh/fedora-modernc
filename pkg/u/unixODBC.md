* [Avoid implicit function declarations, for C99 compatibility](https://github.com/lurcher/unixODBC/pull/138)
* https://src.fedoraproject.org/rpms/unixODBC/c/ad00dfc6fe34f83115540173227131660d620002?branch=rawhide
* [PostgreSQL driver: Fix incompatible pointer-to-integer types](https://github.com/lurcher/unixODBC/pull/157)
* https://src.fedoraproject.org/rpms/unixODBC/c/7f199a018ee0ffa758415f0500c672b7cd38a43c?branch=rawhide
