* [[pgpool-hackers: 4240] Proper declaration for SplitIdentifierString](https://www.pgpool.net/pipermail/pgpool-hackers/2022-December/004241.html)
* [Fix compiler warning.](https://git.postgresql.org/gitweb/?p=pgpool2.git;a=commitdiff;h=3dce610eca07759b05fd1dd39decd77e235cac13)
* https://bugzilla.redhat.com/show_bug.cgi?id=2153791
* https://src.fedoraproject.org/rpms/postgresql-pgpool-II/c/d3b2e605c19bfc9d3bfaee8b8d83f0b08ef57241?branch=rawhide
