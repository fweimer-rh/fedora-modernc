* [configure.ac fix for C99 compatibility](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=60022), [mailing list thread](https://lists.gnu.org/archive/html/bug-guile/2022-12/msg00017.html)
* [Update Gnulib to v0.1-4379-g2ef5a9b4b](https://git.savannah.gnu.org/cgit/guile.git/commit/?id=a91b95cca2d397c84f8b9bbd602d40209a7092ce)
* https://src.fedoraproject.org/rpms/guile22/c/a757ca509c1e5db73ee02013a8acfd90bc5a3605?branch=rawhide
