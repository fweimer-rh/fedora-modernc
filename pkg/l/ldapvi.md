* [[ldapvi] [PATCH] Include <openssl/rand.h> for the RAND_bytes function](http://lists.askja.de/pipermail/ldapvi/2022-December/000126.html)
* [[ldapvi] [PATCH] Remove ldap_str2dn compatibility kludge](http://lists.askja.de/pipermail/ldapvi/2022-December/000127.html)
* [[ldapvi] [PATCH] Do not define _XOPEN_SOURCE in parse.c](http://lists.askja.de/pipermail/ldapvi/2022-December/000128.html)
* [[ldapvi] [PATCH] Declare fdcp in common.h, so that it can be called in sasl.c](http://lists.askja.de/pipermail/ldapvi/2022-December/000129.html)
* https://src.fedoraproject.org/rpms/ldapvi/c/d07a486a6ab30b80dd715276b401d6ddb6a29619?branch=rawhide
* [[PATCH 3/5] return 0 instead of nothing](http://lists.askja.de/pipermail/ldapvi/2023-September/000134.html)
* https://src.fedoraproject.org/rpms/ldapvi/c/4c850bc6e9d1c24a0d904531cd5bf3a15c974c97?branch=rawhide
