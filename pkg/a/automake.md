* [tests: Fix 'type defaults' error in link_cond due to main not being properly declared](https://git.savannah.gnu.org/cgit/automake.git/commit/?id=2a9908da9dbc075ee6c4e853cf3be0365b15f202)
* [[PATCH v2 2/3] tests: Fix implicit function declaration errors](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=59993#23)
* [[PATCH v3] tests: Fix implicit function declaration in ax/depcomp.sh](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=60962#5)
* [[PATCH v2] tests: Don't try to prevent flex to include unistd.h](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=59994#29)
* [tests: depcomp: ensure make_ok() fails when run_make fails](https://git.savannah.gnu.org/cgit/automake.git/commit/?id=6d6fc91c472fd84bd71a1b012fa9ab77bd94efea)
* https://src.fedoraproject.org/rpms/automake/c/12cff5a32336dfdee0cf95f8abf55d020bb3fac8?branch=rawhide
