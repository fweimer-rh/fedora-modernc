* [Simplify & fix check for szlib encoder (#2263)](https://github.com/HDFGroup/hdf5/commit/9dd36f016a4316b94f5df15856d228de01d304c6)
* https://src.fedoraproject.org/rpms/hdf5/c/2008cfd36775c74b0c03afd60a9c492423a025f0?branch=rawhide
