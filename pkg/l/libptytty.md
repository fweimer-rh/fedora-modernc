* [[PATCH] libptytty: Fix implicit function declarations](http://lists.schmorp.de/pipermail/rxvt-unicode/2023q2/002652.html)
* https://src.fedoraproject.org/rpms/libptytty/c/4d2579fb0b427bb048edf7cd8ce042a928ca4548?branch=rawhide
