* [Fix C99 compatibility issue](https://github.com/Xastir/Xastir/pull/204)
* https://src.fedoraproject.org/rpms/xastir/c/d6519828ef8f3be6a051bf03a095bb41308e46d6?branch=rawhide
