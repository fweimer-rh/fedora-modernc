* [Fix C99 compatibility issue](https://github.com/NLnetLabs/nsd/pull/265)
* https://src.fedoraproject.org/rpms/nsd/c/18edafe71c58050867dbc998a5846a9abb406b6c?branch=rawhide
