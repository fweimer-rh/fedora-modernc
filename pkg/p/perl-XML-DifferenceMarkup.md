* [Improve C99 compatibility of library probing](https://rt.cpan.org/Public/Bug/Display.html?id=145911)
* https://src.fedoraproject.org/rpms/perl-XML-DifferenceMarkup/c/686e7bc4591c3edc5595c5f97cdb3b0eccbf5f5b?branch=rawhide
