* [Fix configure.in for clang16](https://github.com/juddmon/jpilot/commit/e5de89bd0766d28ecbc84add404cafaf824ca15e)
* https://src.fedoraproject.org/rpms/jpilot/c/8af35acfe4a45a586893194de281ebd500aea509?branch=rawhide
