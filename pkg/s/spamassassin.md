* [Avoid calling undeclared functions in configure script](https://bz.apache.org/SpamAssassin/show_bug.cgi?id=8076)
* https://src.fedoraproject.org/rpms/spamassassin/c/334f632b160384bb3c8670a9aaf8d5a7f66cdd3e?branch=rawhide
* [Please use more recent autoconf for releases](https://bz.apache.org/SpamAssassin/show_bug.cgi?id=8204)
* https://src.fedoraproject.org/rpms/spamassassin/c/78f14cdc7ace242a955341b8831faad8d636ec8b?branch=rawhide
