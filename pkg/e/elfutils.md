* [[PATCH] testsuite: Avoid C99 compatibility issues in run-native-test.sh](https://sourceware.org/pipermail/elfutils-devel/2023q2/006143.html)
* https://src.fedoraproject.org/rpms/elfutils/c/829b595e08d16f53cf70e56a895983d3f583d56d?branch=rawhide
