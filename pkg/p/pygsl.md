* [Use PyObject_GC_UnTrack in lieu of the old variant](https://github.com/swig/swig/commit/db9822788e183b79457d6bedab8b9a5cabb4cd5e)
* https://src.fedoraproject.org/rpms/pygsl/c/278ed03e18d0cfb37ae9bcbed68071c2cd0bb5d2?branch=rawhide
