* [Building in strict C99 mode](https://github.com/ksh93/ksh/issues/587)
* https://bugzilla.redhat.com/show_bug.cgi?id=2144903
* https://src.fedoraproject.org/rpms/ksh/c/225f16ca87fdfcb6806fc3abd8b5d294bd43e4c9?branch=rawhide
* Also see [the false positives document](/false-positives.md#ksh).

