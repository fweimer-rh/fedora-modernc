* [japanese-bitmap-fonts: Fix C99 compatibility issue in bundled font tool](https://bugzilla.redhat.com/show_bug.cgi?id=2167285)
* https://src.fedoraproject.org/rpms/japanese-bitmap-fonts/c/81ee16789289c144650e73b3ca48a0306bc62dda?branch=rawhide
