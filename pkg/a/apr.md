* [calls to exit() require stdlib.h or else we get -Werror,-Wimplicit-function-declaration](https://svn.apache.org/viewvc?view=revision&revision=1882980)
* [Fix configure for compilers which don't accept implicit int (no longer part of C since C99).](https://svn.apache.org/viewvc?view=revision&revision=1906349)
* [Fix further strict C99 compliance issue. (fixes #37)](https://svn.apache.org/viewvc?view=revision&revision=1906595)
* [Use AC_CHECK_SIZEOF, so as to support cross compiling. PR 56053.](https://svn.apache.org/viewvc?view=revision&revision=1871981)
* [#include <arpa/inet.h> is required for inet_addr](https://svn.apache.org/viewvc?view=revision&revision=1882981)
* [configure.in: Remaining use of implicit int in struct rlimit check](https://bz.apache.org/bugzilla/show_bug.cgi?id=66426)
* https://src.fedoraproject.org/rpms/apr/c/9dd114d0b975813b7e2e35bd06629239c03015e9?branch=rawhide