* [Fix GCC 14 compatibility issues](https://github.com/jwrdegoede/gtk-v4l/pull/2)
* https://src.fedoraproject.org/rpms/gtk-v4l/c/647a0150219e5e865156664dcd9b7c5271ba85a5?branch=rawhide
