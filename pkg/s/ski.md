* [src/ssGtk.c: fix implicit function prototype declarations](https://github.com/trofi/ski/commit/241b7c6268b3adb43a92759850eeeec921ad17c8)
* [ssGtk.c: fix dataStart extern declaration](https://github.com/trofi/ski/commit/027b69d20b1e1c737bd41f0b936aae0055a1e8a1)
* https://src.fedoraproject.org/rpms/ski/c/73a73ca0bf7b8c313335f61ea7e10e01c37d5119?branch=rawhide
* [src/ssGtk.c: fix runItGtk prototype to match gtk-2 API](https://github.com/trofi/ski/commit/332248d54a02540bae215c2f6d58e66ddffbd5ad)
* [src/ssGtk.c: don't cast GtkWidget around](https://github.com/trofi/ski/commit/abc9fe6aef2f34b359b26190e259eae21a936308)
* [src/ssGtk.c: fix assignment to Widget as to int, not pointer](https://github.com/trofi/ski/commit/ac3415c62ac631503bd66eb687e8c40163fdef3c)
* [src/ssGtk.c: assert previse UI types for TLB windows](https://github.com/trofi/ski/commit/9d801f249c4079e6524f4224e69480936ac346df)
* https://src.fedoraproject.org/rpms/ski/c/b0369047ee19117c74b3044a1e7de6bf09f93793?branch=rawhide
