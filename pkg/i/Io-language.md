* [fix EditLine warnings under clang](https://github.com/IoLanguage/io/commit/ad58e1c661874e779f3638bb19090d633badb715)
* [Fix IoImage clang warnings](https://github.com/IoLanguage/io/commit/05ca313f6cf1b5728631fe5ec923eb5078733684)
* [Added missing header to SHA1 library](https://github.com/IoLanguage/io/commit/e707943b623c9bf680ac6a5bd54646ce1b5a61bd)
* [Fixed wrong function call in _IoCairoPDFSurface.c](https://github.com/IoLanguage/io/commit/8cd5b5ad09a7707c859a7bdea342ae7ff2466e3c)
* [Addded preprocessor switch for linux to coroutines](https://github.com/IoLanguage/io/commit/92fe8304c55b84a17b0624613a7006e85a0128a2)
* https://src.fedoraproject.org/rpms/Io-language/c/c2e7ce5247c9cec20fcdeae53ab85a4fbb121b12?branch=rawhide
