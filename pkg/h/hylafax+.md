* [C99 compatibility fixes for configure from Florian Weimer and Fedora](https://sourceforge.net/p/hylafax/HylaFAX+/2704/)
* https://src.fedoraproject.org/rpms/hylafax+/c/21ac179c882b9f21764db19a170a605eac8dde96?branch=rawhide
