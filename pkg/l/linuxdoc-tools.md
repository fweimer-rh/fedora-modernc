* [[PATCH] sgmls: Avoid implicit ints/function declarations in configure](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1034362)
* https://src.fedoraproject.org/rpms/linuxdoc-tools/c/de1d959218cae00c60e2b0f055c57829fe8923e0?branch=rawhide
* [[PATCH] sgmls: Fix type of signal handlers for C89 compatibility](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1061266)
* [sgmls-1.1/*: Deal with C23 making -Wstrict-prototypes error by default.](https://gitlab.com/agmartin/linuxdoc-tools/-/commit/fd6cf2b50d5bd9f013017b53edefe51e0e54f5c4)
* [Silent miscompilation with GCC 14 due to C89 compatibility issue](https://src.fedoraproject.org/rpms/linuxdoc-tools/c/5f7167e287ad57b3e181a8e68096ec9329191165?branch=rawhide)
