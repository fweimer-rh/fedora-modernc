* [Avoid implicit function declarations, for C99 compatibility](https://github.com/jedwing/CHMLib/pull/17)
* https://src.fedoraproject.org/rpms/chmlib/c/84276ac3a1280d8e8474691e6fba38aaa7bf443a?branch=rawhide
