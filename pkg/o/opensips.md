* [Defining __USE_MISC has undefined behaviour](https://github.com/OpenSIPS/opensips/issues/3005)
* [lua: proper include for timegm](https://github.com/OpenSIPS/opensips/commit/acf0c149503b034955557020ea1ad89713cded12)
* https://src.fedoraproject.org/rpms/opensips/c/5a059ceb901b156905bc8a927eccf19346a1a5f9?branch=rawhide
