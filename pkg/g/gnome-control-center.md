* [misc: Fix incompatible type warnings](https://gitlab.gnome.org/GNOME/gnome-control-center/-/commit/13c939659a844049ddc68e90c7830bf96ebe174d)
* https://src.fedoraproject.org/rpms/gnome-control-center/c/47bde83ae9667b9ed33ff47e7fe768541b564158?branch=rawhide
