* [Fix compile error with future versions of gcc](https://github.com/OpenSCAP/openscap/pull/1922)
* https://src.fedoraproject.org/rpms/openscap/c/11558c84eb2418fb9fa4ba55fa61e63b1d2e8e8f?branch=rawhide
* [GCC 14 compatibility fixes](https://github.com/OpenSCAP/openscap/pull/2069)
* https://src.fedoraproject.org/rpms/openscap/c/6e3befd4e16f6ee24f4441d805564b2a22fe5e93?branch=rawhide