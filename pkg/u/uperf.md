* [configure: Fix sendfilev test for better C99 compatibility](https://github.com/uperf/uperf/pull/54)
* https://src.fedoraproject.org/rpms/uperf/c/e29b9ed40ba8351abae9832f96f9c64630ec65c7?branch=rawhide
