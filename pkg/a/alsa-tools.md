* [Fix build failure on Clang 16](https://github.com/alsa-project/alsa-tools/pull/17)
* https://src.fedoraproject.org/rpms/alsa-tools/c/4e028d85fc4bfa6fccd43f269e2497235b29c775?branch=rawhide
