* [Avoid implicit declaration of exit in cmake/modules/TestForHighBitCharacters.c](https://sourceforge.net/p/plplot/patches/37/)
* https://src.fedoraproject.org/rpms/plplot/c/546e9a1261991c46095bff1b6d39e19dc02a6118?branch=rawhide
