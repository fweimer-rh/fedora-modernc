* [configure: Fix __builtin_mul_overflow() compiler support check](https://gitlab.isc.org/isc-projects/bind9/-/merge_requests/7877)
* https://src.fedoraproject.org/rpms/bind9-next/c/76a914813d9cb1a9cc49c2034937fb8c90123500?branch=rawhide
