* [star: Build in C89 mode](https://bugzilla.redhat.com/show_bug.cgi?id=2187168)
* https://src.fedoraproject.org/rpms/star/c/b94f01fdd427785e81db2197b34f47d70f8da543?branch=rawhide
* [Link in build configuration for i686 to use Fedora build flags](https://src.fedoraproject.org/rpms/star/c/a569bd6b4424a94343911cc1a1eb5fb52977ce9f?branch=rawhide)
