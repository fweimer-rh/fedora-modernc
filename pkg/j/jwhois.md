* [C99 compatibility fixes](https://github.com/jonasob/jwhois/pull/35)
* https://src.fedoraproject.org/rpms/jwhois/c/152ae03244b0d0a484eb155ca046f3b85fe4ff05?branch=rawhide (also includes fixes for downstream-only `jwhois-4.0-ipv4_ipv6.patch`)
