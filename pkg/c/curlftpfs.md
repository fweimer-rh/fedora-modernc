* [GCC 14/Clang compatibility fix](https://sourceforge.net/p/curlftpfs/patches/15/)
* https://src.fedoraproject.org/rpms/curlftpfs/c/360799f6e4ca328d604fc505963f3e2809fcd3a1?branch=rawhide
