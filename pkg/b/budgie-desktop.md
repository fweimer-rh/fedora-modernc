* https://bugzilla.redhat.com/show_bug.cgi?id=2179136
* [Use correct C header for meta_keybindings_set_custom_handler](https://github.com/BuddiesOfBudgie/budgie-desktop/commit/aff19926990699e52fcd99318e3489cef711e25b)
* https://src.fedoraproject.org/rpms/budgie-desktop/c/68fd21b3033d4e15ed3813d790d74fbd702ed7be?branch=rawhide
