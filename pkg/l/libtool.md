* [[PATCH] testsuite: Fix C99 compatibility issue in lt_dlopen_a.at](https://lists.gnu.org/archive/html/libtool-patches/2023-01/msg00000.html)
* https://src.fedoraproject.org/rpms/libtool/c/07f4318bf849deb86d9f17bc4f088b98305e3a65?branch=rawhide
