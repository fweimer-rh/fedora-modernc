* [xslt.pxi: calls to _copyXSLT() cast to the wrong type - lxml fails to build with GCC 14](https://bugs.launchpad.net/lxml/+bug/2051243)
* [Reduce the type safety as a workaround for build failures in Fedora 40+](https://src.fedoraproject.org/rpms/python-lxml/c/0f5102956846f0396780ac3ff116c20795dbbf89?branch=rawhide)
