* [Avoid implicit function declarations in the configure script](https://github.com/rbowler/spinhawk/pull/111)
* https://src.fedoraproject.org/rpms/hercules/c/f0ab855c7a7bd423cc731e9d29f9488b582f515f?branch=rawhide
