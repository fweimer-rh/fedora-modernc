* [Fixes for C99 compatibility](https://github.com/fvwmorg/fvwm/pull/100)
* https://src.fedoraproject.org/rpms/fvwm/c/491fffe12f2e8be85d726008684c0adbcdae10a8?branch=rawhide
* https://src.fedoraproject.org/rpms/fvwm/c/85178bd7cf972c5ccce8dabcd8859e359e44e604?branch=rawhide
