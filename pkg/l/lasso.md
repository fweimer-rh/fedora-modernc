* [[lasso] Various fixes for GCC/OpenSSL/libxml2 compatibility](https://listes.entrouvert.com/arc/lasso/2024-01/msg00000.html)
* https://src.fedoraproject.org/rpms/lasso/c/0f5cb2907431eb5290be03a1cb8cd17ba66ff462?branch=rawhide
* https://src.fedoraproject.org/rpms/lasso/c/a8e038a11b13dc9899023e9c6ed027843bca8503?branch=rawhide
* https://src.fedoraproject.org/rpms/lasso/c/87f60a083dca458e7d7476f13d08cc769aec95a9?branch=rawhide
