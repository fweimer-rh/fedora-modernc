* [configure.ac: Include <stdio.h> for printf in __FUNCTION__ check](https://github.com/dajobe/raptor/pull/56)
* [configure.ac: fix -Wimplicit-function-declaration in HAVE___FUNCTION__ test (Clang 16)](https://github.com/dajobe/raptor/commit/69aa78133144fe7276dd4fe7f2eb7ff64f0a8337)
* https://src.fedoraproject.org/rpms/raptor2/c/6a4b46d7c9c2d4b01232e84f88e62731ec1df49f?branch=rawhide
* [Fix error returns in new world methods](https://github.com/dajobe/raptor/commit/567d4d1ab639d924e8d5af459476f331b9af0ce5)
* https://src.fedoraproject.org/rpms/raptor2/c/0be27ecc9473daf62b4cb3798f4b49b09b196119?branch=rawhide
