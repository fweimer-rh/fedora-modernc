* [configure: Avoid implicit ints and implicit function declarations](https://bitbucket.org/icl/papi/pull-requests/406)
* https://bugzilla.redhat.com/show_bug.cgi?id=2148723
* https://src.fedoraproject.org/rpms/papi/c/1c4d45e252ed86e11792b4aeb9903e96b7299be2?branch=rawhide
* [configure: fix tls detection](https://github.com/icl-utk-edu/papi/commit/dd11311aadbd06ab6c76d49a997a8bb2bcdcd5f7)
* [configure: Fix return values in start thread routines](https://github.com/icl-utk-edu/papi/pull/142)
* https://src.fedoraproject.org/rpms/papi/c/3c0ae17fc1d6410ba6cfcf5064efb9b53e0d134e?branch=rawhide
