* [fix for clang from Xcode 12](https://github.com/GlobalArrays/ga/commit/3df34834189ba45a090f1dcba7d71d23f7b11735)
* [more fixes for clang from Xcode 12](https://github.com/GlobalArrays/ga/commit/b444960c8c993f3f5c51acf37451e418e819d007)
* https://src.fedoraproject.org/rpms/ga/c/0767275763ebafea1886b9bb91ca1e428ccbfe2d?branch=rawhide
