* [Fix BOM_codes initializer](https://github.com/mhulden/foma/pull/151)
* https://src.fedoraproject.org/rpms/foma/c/95807b7d13a36d3ce1f72517ddb7f533fb0cde28?branch=rawhide
