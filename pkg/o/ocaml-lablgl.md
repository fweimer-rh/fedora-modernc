* [Fix mismatched pointer types for GCC 14](https://github.com/garrigue/lablgl/pull/6)
* https://src.fedoraproject.org/rpms/ocaml-lablgl/c/bd2f4bded3467f95878ac0809454016d43c9c0ce?branch=rawhide
