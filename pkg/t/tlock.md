* [tlock: Fix missing prototype for crypt (pointer truncation)](https://bugzilla.redhat.com/show_bug.cgi?id=2160647)
* https://src.fedoraproject.org/rpms/tlock/c/1cc281a07080ad559a755e2884c182aa8f794545?branch=rawhide
