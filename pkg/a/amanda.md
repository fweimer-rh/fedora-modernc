* [configure: Avoid an implicit int in the IPv6 test](https://github.com/zmanda/amanda/pull/220)
* https://src.fedoraproject.org/rpms/amanda/c/7a8839fc14ef3702125c5b22b48c6a3fe7e991da?branch=rawhide
* [C compatibility fixes](https://github.com/zmanda/amanda/pull/242)
* https://src.fedoraproject.org/rpms/amanda/c/d9c69a38839b0e095101c9dcc96a0e90bf52945c?branch=rawhide
