* [Enable AC_USE_SYSTEM_EXTENSIONS to avoid implicit function declarations](https://sourceforge.net/p/matio/patches/12/)
* https://src.fedoraproject.org/rpms/matio/c/dc118f730afeee493c95f519fdfad3efbe762f36?branch=rawhide
