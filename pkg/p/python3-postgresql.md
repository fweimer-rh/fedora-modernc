* [buffer.c: Fix incompatible pointer to integer conversion](https://github.com/python-postgres/fe/commit/260d4d42e0c3a2951fe9543dc4e18d93c12950ec)
* [wirestate.c: Fix incompatible pointer to integer conversion](https://github.com/python-postgres/fe/commit/e24e486cd4bf08a36c0c358300ad80817aa279a0)
* https://src.fedoraproject.org/rpms/python3-postgresql/c/3f530f6b6cd51296c82f042e0c81d24253108a92?branch=rawhide
