* [meson: Remove unused ctime_r check](https://gitlab.gnome.org/GNOME/gnome-system-monitor/-/merge_requests/67)
* https://src.fedoraproject.org/rpms/gnome-system-monitor/c/9ee7c87606d8b9e8d0fbd578b85c88376046029c?branch=rawhide
