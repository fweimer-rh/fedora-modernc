* [Changed cppflags test to not always evaluate as false.](http://git.savannah.nongnu.org/cgit/nmh.git/commit/?id=773f15d53348d417fe50d52cfe49f042df4a32ad)
* https://src.fedoraproject.org/rpms/nmh/c/6d9474daf0f66d37d3ab144c4d8b00feeda61a56?branch=rawhide
* https://src.fedoraproject.org/rpms/nmh/c/21b589d04d8badf514f0def54431c4e5dc6c0f32?branch=rawhide
