* [<stdio.h>: Make fopencookie, vasprintf, asprintf available by default](https://sourceware.org/git/?p=glibc.git;a=commitdiff;h=0d5cb2ae27c0a163c15e5222fb132bf9d026b14b)
* https://src.fedoraproject.org/rpms/glibc/c/509721ab91e07222ba857488fe24d419fdce8fe4?branch=rawhide
* Note: Fixed via glibc header file change, no changes to rtpproxy were made.
* [-Wincompatible-pointer-types configure check may fail unconditionally with future compilers](https://github.com/sippy/rtpproxy/issues/149)
* [Include <err.h> if available](https://github.com/sippy/rtpproxy/pull/151)
* https://src.fedoraproject.org/rpms/rtpproxy/c/0ac9c8c4aca392d0dd9d8a2be8ac4e5dd80655c9?branch=rawhide
