* [Bug #145943 for File-RsyncP: C99 compatibility fixes for FileList/configure](https://rt.cpan.org/Public/Bug/Display.html?id=145943)
* https://src.fedoraproject.org/rpms/perl-File-RsyncP/c/18999f1ee9ec657a37b1cf30677a199b9354a238?branch=rawhide
* https://src.fedoraproject.org/rpms/perl-File-RsyncP/c/0faafaa6c4c1ebf930c1c119c57bea85cbd6118b?branch=rawhide


