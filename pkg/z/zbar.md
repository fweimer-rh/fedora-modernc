* [Add missing Py_SIZE to py311.patch](https://src.fedoraproject.org/rpms/zbar/c/107c91c11fd40f9b0d99f02f2bd767df38f99491?branch=rawhide) (bug in downstream-only patch)
