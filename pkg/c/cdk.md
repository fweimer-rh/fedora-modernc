* https://bugzilla.redhat.com/show_bug.cgi?id=2161912
* [Re: C99 compatibility fix for CDK configure script](https://lists.nongnu.org/archive/html/bug-ncurses/2023-01/msg00033.html)
* [snapshot of project "my-autoconf", label t20221202](https://github.com/ThomasDickey/my-autoconf-snapshots/commit/fef663d95d294f48391a43f729f721e42238921b)
* https://src.fedoraproject.org/rpms/cdk/c/917265b5d36a9c185d682ace26497a3fcf6059c3?branch=rawhide
