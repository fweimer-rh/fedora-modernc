* [posix_serialport_impl.c: error: incompatible pointer to integer](https://github.com/hparra/ruby-serialport/pull/75)
* [Cast OBJSETUP argument to VALUE](https://github.com/hparra/ruby-serialport/pull/76)
* https://src.fedoraproject.org/rpms/rubygem-serialport/c/12bac47e7ad541bcdb0d5ac86e7c5ddd9739287f?branch=rawhide
