* [configure: Fix initialization from incompatible pointer type](https://gitlab.com/nbdkit/nbdkit/-/commit/32a9ee6650654469cd591a3ae26842c54f898392)
* https://src.fedoraproject.org/rpms/nbdkit/c/6debad82d3b543e9b44d2d06fd84f98687faa521?branch=rawhide
