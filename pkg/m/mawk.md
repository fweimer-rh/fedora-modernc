* [mawk: configure script does not include <stdlib.h> while probing random functions](https://bugzilla.redhat.com/show_bug.cgi?id=2167291)
* https://src.fedoraproject.org/rpms/mawk/c/16f38b8586b7be28703456aa1eeaecf4f4626cf8?branch=rawhide
