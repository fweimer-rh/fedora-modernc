* [libzia 4.38](http://tucnak.nagano.cz/wiki/Changelog#4.38) fixed
  `configure.ac` to include `<gnu/libc-version.h>` for
  `gnu_get_libc_version` without documenting it in the changelog.
* https://src.fedoraproject.org/rpms/libzia/c/4f3d300b15182ef132adb9c56c84ffcba3befd7a?branch=rawhide
