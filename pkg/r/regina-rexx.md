* [Configure probes for gethostbyname_r and getpwuid_r fail incorrectly with strict compilers](https://sourceforge.net/p/regina-rexx/bugs/588/)
* https://src.fedoraproject.org/rpms/regina-rexx/c/817530dba4b95debd226f324ecf576bc46c72f90?branch=rawhide
