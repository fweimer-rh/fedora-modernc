* [CMakeLists: Avoid implicit function declarations on GNU/Linux](https://github.com/fluent/fluent-bit/pull/6704)
* https://src.fedoraproject.org/rpms/fluent-bit/c/c4a5e1f6ae958c21bccef19f54829b4fe556a0db?branch=rawhide
* [aws_util: fix type of extra_user_agent](https://github.com/fluent/fluent-bit/commit/676d1fa1a830306123da32d73beb55e034971cf1)
* [storage: modified the log_cb prototype to match the one expected by cio](https://github.com/fluent/fluent-bit/commit/4c366df74acee4dfc57c3d0ab7ac8b080d9eaea7)
* [fstore: modified the log_cb prototype to match the one expected by cio](https://github.com/fluent/fluent-bit/commit/5754e9adf0f6999598719105e494a1696c957f7c)
* https://src.fedoraproject.org/rpms/fluent-bit/c/917f94c8db3a3a0953f74bd2aa1753d742fbe06c?branch=rawhide
* [out_stackdriver: removed mbedtls](https://github.com/fluent/fluent-bit/commit/0f5562bded73c425260c2c6975ec5919d48ffa83)
* [out_stackdriver: adjust test formatter callback params](https://github.com/fluent/fluent-bit/commit/11bdc5eed94da167643a05a59b88dfc3c2d7c3bb)
* https://src.fedoraproject.org/rpms/fluent-bit/c/7d7be4c8edf3506f23610c853e1bc0a96e4e7c9c?branch=rawhide
