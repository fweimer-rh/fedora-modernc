* [Fix implicit function declarations in the Configure script](https://github.com/gtk-gnutella/gtk-gnutella/pull/38)
* https://src.fedoraproject.org/rpms/gtk-gnutella/c/de85ee858777e43261fcce5c60660ca39f2f3d1d?branch=rawhide
