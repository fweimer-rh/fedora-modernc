* [8307542: Call to FcConfigAppFontAddFile uses wrong prototype, arguments](https://github.com/openjdk/jfx/pull/1128)
* https://bugs.openjdk.org/browse/JDK-8307542
* [8323077: C type error (incompatible function pointer) in X11GLContext.c](https://github.com/openjdk/jfx/pull/1319)
* https://bugs.openjdk.org/browse/JDK-8323077
* [8323078: Incorrect length argument to g_utf8_strlen in pango.c](https://github.com/openjdk/jfx/pull/1320)
* https://bugs.openjdk.org/browse/JDK-8323078
* https://src.fedoraproject.org/rpms/openjfx/c/9e893037f3a20273c79603220b7a18a44be60fcc?branch=rawhide
