* [Avoid relying on C89 features in a few places](https://gitlab.com/samba-team/samba/-/merge_requests/2807)
* https://src.fedoraproject.org/rpms/libtevent/c/b1ef01db8bb0798a9af58d8ac20d0228c99662a7?branch=rawhide
