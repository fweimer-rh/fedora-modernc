* [hugs98: Needs C89 features (implicit function declarations, implicit ints) and C99 inlining mode](https://bugzilla.redhat.com/show_bug.cgi?id=2160645)
* [Set build_type_safety_c to 0 (#2160645)](https://src.fedoraproject.org/rpms/hugs98/c/36c5ca15c677ec74f3e5b866aa294a911a25e1e5?branch=rawhide)
* [Build with CC="gcc -fpermissive", increase command line buffer size](https://src.fedoraproject.org/rpms/hugs98/c/0763f60047a09e0b1717ce90cd354c0ebbea691f?branch=rawhide)
* Note: `hugs98-machdep-bufsize.patch` is needed to increase a static buffer size due to the length of the command line, to avoid a failure, _ERROR "libraries/bootlib/Foreign/Marshal/Alloc.hs" - Unable to build compilation command_.
