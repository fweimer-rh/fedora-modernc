* [ncl: C99 port or switch language mode to C89](https://bugzilla.redhat.com/show_bug.cgi?id=2145150)
* [Choice of C language mode: C99 on Linux despite C99 incompatibilities](https://github.com/NCAR/ncl/issues/193)
* [Set build_type_safety_c to 0 (#2145150)](https://src.fedoraproject.org/rpms/ncl/c/881202de9a56c90cd3cf57e304775988c0be393f?branch=rawhide)
