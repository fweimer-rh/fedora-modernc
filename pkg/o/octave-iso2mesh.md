* [Avoid implicit function declarations in nl_single_file.c](https://github.com/fangq/meshfix/pull/1)
* https://src.fedoraproject.org/rpms/octave-iso2mesh/c/3c9270516c1fb07420e7b94d2fd9743d2c2e4672?branch=rawhide
