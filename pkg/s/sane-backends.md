* [acinclude.m4: fix -Wimplicit-function-declaration](https://gitlab.com/sane-project/backends/-/commit/300b460970f538ab515835f14650785e88808a8f)
* https://src.fedoraproject.org/rpms/sane-backends/c/c555c50533ba439b5c6b2dfc0be1ebef9e0648e2?branch=rawhide
