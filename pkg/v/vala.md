* [Missing declarations of called C functions in [ModuleInit] function](https://gitlab.gnome.org/GNOME/vala/-/issues/1422)
* [codegen: Add declaration for register call of dynamic DBus interfaces](https://gitlab.gnome.org/GNOME/vala/-/commit/a902d7eae96a3f1ab0cb64f268443b23ee8ab45a)
* https://src.fedoraproject.org/rpms/vala/c/f57c17223228a505cb166e814f631d4fbb7fe24b?branch=rawhide
* [codegen: Emit GCC diagnostics pragmata for GCC 14 compatibility](https://gitlab.gnome.org/GNOME/vala/-/merge_requests/369)
* [Downgrade GCC 14 C type errors to warnings](https://src.fedoraproject.org/rpms/vala/c/6c9c11e678f5c693c437fa73ccb5cb0c2d33fc84?branch=rawhide)
* [Downgrade GCC 14 -Wint-conversion errors to warnings](https://src.fedoraproject.org/rpms/vala/c/67fbbfc5e7b859ad8081c8332f982c83ceec922a?branch=rawhide)
