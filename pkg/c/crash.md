* [[Crash-utility] [PATCH] Fix C99 compatibility issues in embedded copy of GDB](https://listman.redhat.com/archives/crash-utility/2023-February/010460.html)
* https://bugzilla.redhat.com/show_bug.cgi?id=2157576
* https://src.fedoraproject.org/rpms/crash/c/f24b8df61ccf8621c40bac2576489794ec1f22af?branch=rawhide
