* [meson: C type error in strtod_l/strtof_l probe](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/26927)
* https://src.fedoraproject.org/rpms/mesa/c/beecf70924105f744adce1f4bed1cf9f6b54228f?branch=rawhide
