* [configure.ac: Improve C99 compatibility of __progname check](https://gitlab.freedesktop.org/libbsd/libbsd/-/merge_requests/23)
* https://src.fedoraproject.org/rpms/libbsd/c/52289875236e1ccc6504a039d0d53bc6485e2072?branch=rawhide
