* [configure: Define _GNU_SOURCE when checking for pthread_setname_np](https://github.com/rakshasa/libtorrent/pull/237)
* https://src.fedoraproject.org/rpms/libtorrent/c/f077e4a0a80e8dfd3fc6fdbe839d87e2934072ae?branch=rawhide
* [Fix compiler warnings. (#204)](https://github.com/rakshasa/libtorrent/commit/b57ca6174e705cb5cfb3dc3b1ac02558d1750bac)
* https://src.fedoraproject.org/rpms/libtorrent/c/0323b506d029404d0a7e33e78062d12f74e8539a?branch=rawhide
