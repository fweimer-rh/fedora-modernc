* [configure.in contains a check that isn't C99 compliant as it's main function lacks a return type.](https://github.com/FreeRADIUS/freeradius-client/issues/113)
* https://src.fedoraproject.org/rpms/freeradius-client/c/12ee8b567ce2a1b6637ad01e5806a15eeef410cc?branch=rawhide
