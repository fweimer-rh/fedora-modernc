* [Fix implicit function declarations (for C99 compatibility)](https://github.com/FabLeo451/lterm/pull/2)
* https://src.fedoraproject.org/rpms/lterm/c/e3cdf179a67c836ddaa70b8b69030c033920a621?branch=rawhide
* https://src.fedoraproject.org/rpms/lterm/c/f939e0ace459f7c0c45cf44cced86fa08a69a02e?branch=rawhide
