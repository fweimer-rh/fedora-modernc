* [Fix C compatibility issue in droplist_handle_plot call](https://github.com/laserjock/plotdrop/pull/4)
* https://src.fedoraproject.org/rpms/plotdrop/c/b1ed0f7d3264286d9d53c02bffebe470db77282c?branch=rawhide
