* [tests: fix test failures on upcoming gcc-14](https://github.com/python-poetry/poetry-core/commit/c08cd149e272e23c2dd77564bd5a2a8ed97098f7)
* https://src.fedoraproject.org/rpms/python-poetry-core/c/28ce50e7383ba817f41484f86f7cfff0f0e94e58?branch=rawhide
