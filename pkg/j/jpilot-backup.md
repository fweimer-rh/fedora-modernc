* [Fix configure.in for clang16](https://github.com/juddmon/jpilot/commit/e5de89bd0766d28ecbc84add404cafaf824ca15e)
* https://src.fedoraproject.org/rpms/jpilot-backup/c/68c94155296bff942f629ad16ba61143d8749604?branch=rawhide
