* [give coin.m4 a fresh start, see also #95](https://github.com/coin-or-tools/BuildTools/commit/f0363fe4ef8b11940138a39fb90ec53717e0b5de) (repository with the source code for `coin.m4`)
* [Bring Cbc into the autotools-update world. Rough, many options untested.](https://github.com/coin-or/Cbc/commit/6ac2bc2f9dfa464869005e05f549631757c3f88d)
* https://src.fedoraproject.org/rpms/coin-or-Cbc/c/aa21fcc83d9dfe5f9a6bd8649278fd9b590b0919?branch=rawhide
