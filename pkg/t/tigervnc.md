* [Remove INITARGS from xserver119.patch](https://github.com/TigerVNC/tigervnc/commit/e4ec992984983ac9107efae1f1cc552007e4229e)
* [C compatibility fixes](https://github.com/TigerVNC/tigervnc/pull/1698)
* https://src.fedoraproject.org/rpms/tigervnc/c/2957f63ae94a44084424568cd15a621ddbc715ae?branch=rawhide (-Wdeclaration-missing-parameter-type fix)
* https://src.fedoraproject.org/rpms/tigervnc/c/b9b7e9d49de8089099292319de43a7357181e611?branch=rawhide (one patch was broken)