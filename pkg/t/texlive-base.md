* [stdlib.h to placate clang16](https://tug.org/svn/texlive?view=revision&revision=64941)
* [void main, void args, more clang16 from sam](https://tug.org/svn/texlive?view=revision&revision=64953)
* [synctexdir: update from GH](https://tug.org/svn/texlive?view=revision&revision=65582)
* [Fix implicit declaration of vasprintf](https://github.com/jlaurens/synctex/commit/2897465154892a7737dcc90e4d6a00a1d1b3922c)
* [xdvik: Fix setsid-after-vfork configure test for C99 compatibility](https://tug.org/pipermail/tex-live/2023-January/048833.html)
* https://src.fedoraproject.org/rpms/texlive-base/c/963e9ad7f25230d3e3c2e50f3ea303540b961033?branch=rawhide
