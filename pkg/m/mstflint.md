* [Add additional #include directives for C99 compatibility](https://github.com/Mellanox/mstflint/pull/774)
* https://src.fedoraproject.org/rpms/mstflint/c/f0ce58d61d2c0aa44dcac630f7266f4889f4f4bd?branch=rawhide
* https://src.fedoraproject.org/rpms/mstflint/c/2cd40784ba0ac148230d59190b12b4e7f5ab206e?branch=rawhide
