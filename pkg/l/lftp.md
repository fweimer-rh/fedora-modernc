* [Fix C99 compatibility issue](https://github.com/lavv17/lftp/pull/693)
* https://src.fedoraproject.org/rpms/lftp/c/9405753eec97a8ce5876e1605cf98429dc5b470b?branch=rawhide
