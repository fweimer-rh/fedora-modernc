* [Ensure mergesort is declared if needed (#7411)](https://github.com/GenericMappingTools/gmt/commit/42198756233ea06c38120d290c693a4acfe7bb59)
* https://src.fedoraproject.org/rpms/GMT/c/d65c488c3c8a26222f44b683c07c8a6ca5d06181?branch=rawhide

