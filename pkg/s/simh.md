* [Fixed in rawhide](https://bugzilla.redhat.com/show_bug.cgi?id=2047013)
* [Patch modernizing large file support preprocessor conditions](https://src.fedoraproject.org/rpms/simh/c/8650beeb1f7f2810a5bc3b7f996928eb53bda2cd?branch=rawhide)
