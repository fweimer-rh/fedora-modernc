* [configure: Fix m4 quoting issue in libargp probe](https://github.com/nhorman/rng-tools/pull/205)
* https://src.fedoraproject.org/rpms/rng-tools/c/fe32a19f907578adbe7022260d3588728f3ef050?branch=rawhide
