* [kasumi: Fix C89 compatibility issue around gui_cell_renderer_spin_start_editing](https://bugzilla.redhat.com/show_bug.cgi?id=2259428)
* https://src.fedoraproject.org/rpms/kasumi/c/dfe21ca85c838220d04b3080c33c6666c6ccc11d?branch=rawhide
