* [Replace 'ugene_custom_open' with a standard 'open'](https://github.com/ugeneunipro/ugene/pull/1034)
* https://src.fedoraproject.org/rpms/ugene/c/06a445cd086f0b7775e2d193e47748218ba1ee4d?branch=rawhide
