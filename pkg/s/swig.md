* [CCache: Do not rely on C89-only features in configure.ac](https://github.com/swig/swig/pull/2483)
* https://src.fedoraproject.org/rpms/swig/c/f50f0e420e6bb9d5c9011049922e6729975cbdae?branch=rawhide
