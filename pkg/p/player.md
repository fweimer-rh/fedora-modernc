* [player: Python module playerc does not work](https://bugzilla.redhat.com/show_bug.cgi?id=2161923)
* [Python 3 support](https://github.com/playerproject/player/issues/17)
* https://src.fedoraproject.org/rpms/player/c/7d742083fa4fa96f1ed4f03518d8e8b680d78476?branch=rawhide
