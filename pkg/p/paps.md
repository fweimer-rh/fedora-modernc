* [Fix build errors](https://github.com/dov/paps/commit/d2f20a14a9930c36005f186eff7ff01137527fca)
* The call to `g_vasprintf` reference is from a downstream-specific patch (`paps-0.6.6-lcnumeric.patch`).
* https://src.fedoraproject.org/rpms/paps/c/c2bb76914bf688903a2f3aa3daa76d4730e07784?branch=rawhide
* [Fix C type error in paps 0.6.8](https://src.fedoraproject.org/rpms/paps/c/10c01b83d58fa23b9963a904a81b3fd92ae222b8?branch=rawhide) (updates downstream-only ` paps-fix-cpi.patch` patch with a real bug fix, not relevant to current upstream)
