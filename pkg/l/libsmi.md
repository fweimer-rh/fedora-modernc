* [C99 compatibility fixes](https://lists.ibr.cs.tu-bs.de/hyperkitty/list/libsmi@ibr.cs.tu-bs.de/thread/MI6XPAR7JE2AY6UZIMXA3Q7JGD635JRR/)
* https://src.fedoraproject.org/rpms/libsmi/c/626632f933d7e80532fc2c37655272ed04dfbfa5?branch=rawhide
* [libsmi: Suspicious type error in Bison parser will lead to build failure](https://bugzilla.redhat.com/show_bug.cgi?id=2256912)
* [C type-safety level downgrade due to pointer/int conversion bugs (#2256912)](https://src.fedoraproject.org/rpms/libsmi/c/1f241e80cf965c512b52936d58f7ad24b3099e74?branch=rawhide)
