* [0009554: C compatibility issue in localtime_r probe](https://tracker.ardour.org/view.php?id=9554)
* [Fix localtime_r check (#9554)](https://github.com/Ardour/ardour/commit/a8c26dbfa4e765e40bbf741637a3e383eebd770a)
* https://src.fedoraproject.org/rpms/ardour7/c/e4e5f01a5f3f4129208312655aecc952610c7df6?branch=rawhide
