* [Crypt::DES fails to build with Xcode 12](https://rt.cpan.org/Ticket/ModifyAll.html?id=133363)
* [Crypt::DES cpan module](https://rt.cpan.org/Ticket/Display.html?id=133412)
* https://src.fedoraproject.org/rpms/perl-Crypt-DES/c/0a4557f6b118387730b895037e4a17c90f212e68?branch=rawhide

