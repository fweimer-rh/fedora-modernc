* [acinclude.m4: Include <stdlib.h> when using exit](https://github.com/oetiker/rrdtool-1.x/commit/f3334fcbde0a77aa48938ba38602bff91c09cf85)
* https://src.fedoraproject.org/rpms/rrdtool/c/c6326f8897fa14a3cb204b9c20358503eb8f0d00?branch=rawhide
* [Constify argv, fix warnings. (#1242)](https://github.com/oetiker/rrdtool-1.x/commit/b76e3c578f1e9f582e9c28f50d82b1f569602075)
* https://src.fedoraproject.org/rpms/rrdtool/c/508fac528549a3e31c23948aaa57bc01a709863c?branch=rawhide
