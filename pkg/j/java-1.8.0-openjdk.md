* [java-1.8.0-openjdk: C99 compatibility fix for LinuxNativeDispatcher.c](https://bugzilla.redhat.com/show_bug.cgi?id=2152432)
  The upstream fix is difficult to locate, but later OpenJDK versions
  do not have this issue.
* https://src.fedoraproject.org/rpms/java-1.8.0-openjdk/c/a16f84b4ee4daec0ad1f5c30db5d78a8adef9813?branch=rawhide
