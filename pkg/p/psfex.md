* [ Fixed regression in sample.c.  Added missing include in catout.c.](https://github.com/astromatic/psfex/commit/c991bb2ab46c0c02b917954decb21136576b289e)
* https://src.fedoraproject.org/rpms/psfex/c/d15a5807f251f531615abcff24c455107e28efe6?branch=rawhide
