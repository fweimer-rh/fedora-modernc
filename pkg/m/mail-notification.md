* https://bugzilla.redhat.com/show_bug.cgi?id=2253950
* [jb_openssl_check, jb_sasl2_check C99 compatibility](https://bugs.launchpad.net/mail-notification/+bug/1997992)
* https://src.fedoraproject.org/rpms/mail-notification/c/e4095a90a6423dd239452744784d1f8cdf1425d1?branch=rawhide
* https://src.fedoraproject.org/rpms/glabels/c/6acc3ad5f63cab322c1aa5a8cd3d5f84a917922b?branch=rawhide
