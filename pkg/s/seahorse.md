* [Avoid C99 incompatibility around seahorse_pkcs11_backend_initialize](https://gitlab.gnome.org/GNOME/seahorse/-/merge_requests/214)
* https://src.fedoraproject.org/rpms/seahorse/c/7b0d8ac0f0a6a00f3b8330ce1364b2744a2e1766?branch=rawhide
