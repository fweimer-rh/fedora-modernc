* [Python 3 does not have a tp_print member in PyTypeObject](https://github.com/ptesarik/libkdumpfile/pull/78)
* https://src.fedoraproject.org/rpms/libkdumpfile/c/fc50c2818970b25041afcea8a7d44579b12fda51?branch=rawhide
