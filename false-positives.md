Usually, implicit function declarations are handled via
`redhat-rpm-config` exceptions:

* https://src.fedoraproject.org/rpms/redhat-rpm-config/blob/private-f38-toolchain/f/report-gcc-errors.lua

(The branch is called `private-f38-toolchain`, but it is actually used
for rawhide builds.)

However, these exceptions only work for implicit function
declarations.  Other obsolete constructs do not have an identifier
associated with them in a meaningful way.

# amanda

The configure script compiles this fragment:

```
int main()
{
#ifdef FD_SET_POINTER
	(void)select(0, (fd_set *) 0, (fd_set *) 0, (fd_set *) 0, 0);
#else
	(void)select(0, (int *) 0, (int *) 0, (int *) 0, 0);
#endif
	return 0;
}
```

This causes incompatible-pointer-types errors for the `int *` variant.
The logic in `config/amanda/funcs.m4` correctly selects the `fd_set *`
variant, although it does try to compile both.

# ccze

This probe fails at link time because the `suboparg` variable does not
exist:

```
AC_MSG_CHECKING(for suboptarg)
AC_RUN_IFELSE(AC_LANG_PROGRAM(
[[#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static char **empty_subopts[[]] = { NULL };
extern char *suboptarg;
]],
[[  char *subopts = "something";
  char *value;

  getsubopt (&subopts, empty_subopts, &value);
  exit (!suboptarg);]]), [have_suboptarg="yes"],
[have_suboptarg="no"], [have_suboptarg="no"])
AC_MSG_RESULT(${have_suboptarg})
case ${have_suboptarg} in
	yes)
		AC_DEFINE_UNQUOTED(HAVE_SUBOPTARG, 1,
			Define this if you have the suboptarg variable)
		;;
esac
```

It also reports an incompatible-pointer-types error in the second
argument to `getsubopt`, but that is not important.

# collectd

A configure test fails like this:

```
conftest.c: In function 'main':
conftest.c:120:29: error: storage size of 'mt' isn't known
  120 |               struct mnttab mt;
      |                             ^~
conftest.c:123:24: error: too many arguments to function 'getmntent'
  123 |               status = getmntent (fh, &mt);
      |                        ^~~~~~~~~
In file included from ./src/utils/mount/mount.h:39,
                 from conftest.c:114:
/usr/include/mntent.h:69:23: note: declared here
   69 | extern struct mntent *getmntent (FILE *__stream) __THROW;
      |                       ^~~~~~~~~
conftest.c:123:22: error: assignment to 'int' from 'struct mntent *' makes integer from pointer without a cast
  123 |               status = getmntent (fh, &mt);
      |                      ^
conftest.c:120:29: warning: unused variable 'mt' [-Wunused-variable]
  120 |               struct mnttab mt;
      |                             ^~
```

This is harmless because of the preceding error concerning `mt`.

# DevIL

With default GCC 13, this program compiles and link with an
`-Wincompatible-pointer-types` warning:

```
# ifdef HAVE_GL_GLU_H
#   include <GL/glu.h>
# else
#   include <OpenGL/glu.h>
# endif
int
main ()
{
GLvoid (*func)(...); gluTessCallback(0, 0, func)
  ;
  return 0;
}
```

However, this check really is expected to fail with our headers
because the real `gluTessCallback` ptototype uses `(void)`, not
`(...)`.  This may lead to an ABI mismatch on some architectures.
The GCC 14 behavior should fix that.

# dialog, lynx, mawk, vile, ncurses

There's a failing configure probe:

```
configure: In function 'main':
configure:6009:42: error: conflicting types for 'readdir'; have 'struct dirent64 *(DIR *)'
 6009 |                 /* if transitional largefile support is setup, this is true */
      |                                          ^~~~~~~
In file included from configure:6002:
/usr/include/dirent.h:164:23: note: previous declaration of 'readdir' with type 'struct dirent *(DIR *)'
  164 | extern struct dirent *readdir (DIR *__dirp) __nonnull ((1));
      |                       ^~~~~~~
configure:6011:36: error: initialization of 'struct dirent *' from incompatible pointer type 'struct dirent64 *'
 6011 |                 struct dirent64 *x = readdir((DIR *)0);
      |                                    ^~~~~~~
configure:6012:27: error: invalid operands to binary - (have 'struct dirent64 *' and 'struct dirent *')
 6012 |                 struct dirent *y = readdir((DIR *)0);
      |                           ^
```

But the probe source turns the warning into an error on older GCC,
too, so this isn't a problem:

```
#line 5998 "configure"
#include "confdefs.h"

#pragma GCC diagnostic error "-Wincompatible-pointer-types"
#include <sys/types.h>
#include <dirent.h>

int
main (void)
{

                /* if transitional largefile support is setup, this is true */
                extern struct dirent64 * readdir(DIR *);
                struct dirent64 *x = readdir((DIR *)0);
                struct dirent *y = readdir((DIR *)0);
                int z = x - y;
                (void)z;

  ;
  return 0;
}
```

# esound

We see an int-conversion error:

```
configure:19593: gcc -o conftest -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer  -Werror  -Wl,-z,relro -Wl,--as-needed  -Wl,-z,now -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -Wl,--build-id=sha1 -specs=/usr/lib/rpm/redhat/redhat-package-notes  conftest.c -lasound  >&5
conftest.c: In function 'main':
conftest.c:57:56: error: passing argument 3 of 'snd_pcm_hw_params_set_rate_near' makes pointer from integer without a cast
   57 |           snd_pcm_hw_params_set_rate_near(pcm, params, val, &dir);
      |                                                        ^~~
      |                                                        |
      |                                                        unsigned int
In file included from /usr/include/alsa/asoundlib.h:56,
                 from conftest.c:46:
/usr/include/alsa/pcm.h:825:96: note: expected 'unsigned int *' but argument is of type 'unsigned int'
  825 | int snd_pcm_hw_params_set_rate_near(snd_pcm_t *pcm, snd_pcm_hw_params_t *params, unsigned int *val, int *dir);
      |                                                                                  ~~~~~~~~~~~~~~^~~
configure:19593: $? = 1
```

Bit it is harmess because the probe already uses `-Werror`:

```
      dnl Check if ALSA uses new API
      saved_cflags="$CFLAGS"
      CFLAGS="$CFLAGS -Werror"
      echo -n "checking for alsa new PCM API... "
      AC_TRY_LINK([
	  #include <alsa/asoundlib.h>
	  ], [
	  snd_pcm_t *pcm;
	  snd_pcm_hw_params_t *params;
	  unsigned int val;
	  int dir;
	  /* intentionally not uses pointers, trying to replicate bug 129709 */
	  snd_pcm_hw_params_set_rate_near(pcm, params, val, &dir);
	  ],[ echo "no" ], AC_DEFINE(DRIVER_ALSA_09_NEW_PCM_API, 1, [Defined if alsa-0.9 new pcm api is detected]) [ echo "yes" ],
	  ])
```

# glib2

A probe for `size_t` fails (as expected), but this harmless because it
is compiled with `-Werror`:

```
Running compile:
Working directory:  …-build/meson-private/tmpbt3hj8xj
Code:
 #include <stddef.h>
        size_t f (size_t *i) { return *i + 1; }
        int main (void) {
          unsigned long long i = 0;
          f (&i);
          return 0;
        }
-----------
Command line: `gcc …-build/meson-private/tmpbt3hj8xj/testfile.c -o …-build/meson-private/tmpbt3hj8xj/output.obj -c -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1 -m64 -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer -D_FILE_OFFSET_BITS=64 -O0 -std=gnu99 -Werror` -> 1
stderr:
…-build/meson-private/tmpbt3hj8xj/testfile.c: In function 'main':
…-build/meson-private/tmpbt3hj8xj/testfile.c:5:14: error: passing argument 1 of 'f' from incompatible pointer type
    5 |           f (&i);
      |              ^~
      |              |
      |              long long unsigned int *
…-build/meson-private/tmpbt3hj8xj/testfile.c:2:27: note: expected 'size_t *' {aka 'long unsigned int *'} but argument is of type 'long long unsigned int *'
    2 |         size_t f (size_t *i) { return *i + 1; }
      |                   ~~~~~~~~^
-----------
Checking if "GCC size_t typedef is long long" compiles: NO 
```

# gpsd

This scons probe:

```
scons: Configure: Checking if strerror_r() returns int... 
.sconf_temp/conftest_1d61eab1ffed995e90ff3652a575a466_0.c <-
  |
  |        #define _GNU_SOURCE
  |
  |        #include <stddef.h>
  |        #include <string.h>
  |
  |        int main() {
  |            char buf[100];
  |            int ret;
  |
  |            ret = strerror_r(24, buf, sizeof(buf));
  |            return ret;
  |        }
  |    
```

fails with:

```
.sconf_temp/conftest_1d61eab1ffed995e90ff3652a575a466_0.c: In function ‘main’:
.sconf_temp/conftest_1d61eab1ffed995e90ff3652a575a466_0.c:11:17: error: assignment to ‘int’ from ‘char *’ makes integer from pointer without a cast
   11 |             ret = strerror_r(24, buf, sizeof(buf));
      |                 ^
scons: Configure: no
```

This is not a problem because the probe is compiled with `-Werror`.

# kdelibs3

A probe checks for `setgroups` with a `short *` argument:

```
conftest.c: In function 'main':
conftest.c:243:30: error: passing argument 2 of 'getgroups' from incompatible pointer type
  243 |             if (getgroups(1, x) == 0) if (setgroups(1, x) == -1) exit(1);
      |                              ^
      |                              |
      |                              short int *
In file included from /usr/include/features.h:503,
                 from /usr/include/grp.h:25,
                 from conftest.c:236:
/usr/include/bits/unistd.h:113:1: note: expected '__gid_t *' {aka 'unsigned int *'} but argument is of type 'short int *'
  113 | __NTH (getgroups (int __size, __gid_t __list[]))
      | ^~~~~
conftest.c:243:56: error: passing argument 2 of 'setgroups' from incompatible pointer type
  243 |             if (getgroups(1, x) == 0) if (setgroups(1, x) == -1) exit(1);
      |                                                        ^
      |                                                        |
      |                                                        short int *
/usr/include/grp.h:180:50: note: expected 'const __gid_t *' {aka 'const unsigned int *'} but argument is of type 'short int *'
  180 | extern int setgroups (size_t __n, const __gid_t *__groups) __THROW;
      |                                   ~~~~~~~~~~~~~~~^~~~~~~~
conftest.c:245:30: error: passing argument 2 of 'getgroups' from incompatible pointer type
  245 |             if (getgroups(1, x) == -1) exit(1);
      |                              ^
      |                              |
      |                              short int *
/usr/include/bits/unistd.h:113:1: note: expected '__gid_t *' {aka 'unsigned int *'} but argument is of type 'short int *'
  113 | __NTH (getgroups (int __size, __gid_t __list[]))
      | ^~~~~
conftest.c:248:30: error: passing argument 2 of 'getgroups' from incompatible pointer type
  248 |             if (getgroups(1, x) == -1) exit(1);
      |                              ^
      |                              |
      |                              short int *
/usr/include/bits/unistd.h:113:1: note: expected '__gid_t *' {aka 'unsigned int *'} but argument is of type 'short int *'
  113 | __NTH (getgroups (int __size, __gid_t __list[]))
      | ^~~~~
configure:38728: $? = 1
```

This probe previously failed at run time.

# ksh

Some issues remain.  The `wcwidth` function is found eventually.  The
other test does not materially affect the build (no textual
differences in header files).

```
./Fbffe1620.c:38: implicit-function-declaration: wcwidth
./Fbffe1620.c:45: implicit-function-declaration: wcwidth
./bffe15400.c:8: int-conversion
./bffe15400.c:8: incompatible-pointer-types
./bffe15400.c:8: incompatible-pointer-types
```

# libgcrypt

This test fragment triggers are deliberate incompatible-pointer-types
error.  It is always compiled with `-Werror`, so there is no problem.

```
void *test(void) {
                 void *(*def_func)(void) = test;
                 void *__attribute__((ms_abi))(*msabi_func)(void);
                 /* warning on SysV abi targets, passes on Windows based targets */
                 msabi_func = def_func;
                 return msabi_func;
             }
```

# libsigsegv

A probe fails due to an incompatible-pointer-types error, but there is
a secondary error (misuse of `->`), so that first error does not
matter:

```
configure:15405: checking whether a fault handler according to MacOSX/Darwin7 Po
werPC works
configure:15553: gcc -o conftest -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer   -Wl,-z,relro -Wl,--as-needed  -Wl,-z,now -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -Wl,--build-id=sha1 -specs=/usr/lib/rpm/redhat/redhat-package-notes  conftest.c  >&5
In file included from conftest.c:38:
./src/fault-macosdarwin7-powerpc.c: In function 'get_fault_addr':
./src/fault-macosdarwin7-powerpc.c:43:22: error: invalid type argument of '->' (have 'mcontext_t')
   43 |     &ucp->uc_mcontext->ss.r0; /* r0..r31 */
      |                      ^~
./src/fault-macosdarwin7-powerpc.c:130:10: warning: cast to pointer from integer of different size [-Wint-to-pointer-cast]
  130 |   return (void *) addr;
      |          ^
conftest.c: In function 'main':
conftest.c:117:21: error: assignment to 'void (*)(int,  siginfo_t *, void *)' from incompatible pointer type 'void (*)(int,  siginfo_t *, ucontext_t *)'
  117 | action.sa_sigaction = &sigsegv_handler;
      |                     ^
```

# libvirt-cim, libcmpiutil

There is an autoconf probe that uses
`-Werror=incompatible-pointer-types` in non-instrumented builds, too
(not shown here):

```
configure:12264: checking return type for indications
configure:12288: gcc -c -Werror  -DCMPI_VERSION=100 -I/usr/include/Pegasus/Provider/CMPI conftest.c >&5
conftest.c: In function 'main':
conftest.c:57:40: error: assignment to 'CMPIStatus (*)(CMPIIndicationMI *, const CMPIContext *)' {aka 'struct _CMPIStatus (*)(struct _CMPIIndicationMI *, const struct _CMPIContext *)'} from incompatible pointer type 'void (*)(CMPIIndicationMI *, const CMPIContext *)' {aka 'void (*)(struct _CMPIIndicationMI *, const struct _CMPIContext *)'}
   57 |                   ft.enableIndications = ei;
      |                                        ^
configure:12288: $? = 1
```

The `libcmpiutil` package uses `-Werror`, too.


# minidlna, openbabel

There is a probe that already uses `-Werror` in `minidlna`:

```
configure:11165: gcc -c -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer  -Wall -Werror  -I/usr/include/ffmpeg conftest.c >&5
conftest.c: In function 'main':
conftest.c:115:39: error: passing argument 3 of 'scandir' from incompatible pointer type
  115 |             (void)scandir(name, &ptr, filter, alphasort);
      |                                       ^~~~~~
      |                                       |
      |                                       int (*)(struct dirent *)
In file included from conftest.c:106:
/usr/include/dirent.h:259:27: note: expected 'int (*)(const struct dirent *)' but argument is of type 'int (*)(struct dirent *)'
  259 |                     int (*__selector) (const struct dirent *),
      |                     ~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

The `openbabel` package has a similar probe, but for CMake:

```
        Building C object CMakeFiles/cmTC_d375e.dir/src.c.o
        /usr/bin/gcc -DSCANDIR_NEEDS_CONST  -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer -Werror -fPIE -o CMakeFiles/cmTC_d375e.dir/src.c.o -c /builddir/build/BUILD/openbabel-openbabel-3-1-1/x86_64-redhat-linux-gnu/CMakeFiles/CMakeScratch/TryCompile-NT3cvU/src.c
        /builddir/build/BUILD/openbabel-openbabel-3-1-1/x86_64-redhat-linux-gnu/CMakeFiles/CMakeScratch/TryCompile-NT3cvU/src.c: In function ‘main’:
        /builddir/build/BUILD/openbabel-openbabel-3-1-1/x86_64-redhat-linux-gnu/CMakeFiles/CMakeScratch/TryCompile-NT3cvU/src.c:7:46: error: passing argument 3 of ‘scandir’ from incompatible pointer type
            7 |      int count = scandir ("./", &entries_pp, matchFiles, 0);
              |                                              ^~~~~~~~~~
              |                                              |
              |                                              int (*)(struct dirent *)
        In file included from /builddir/build/BUILD/openbabel-openbabel-3-1-1/x86_64-redhat-linux-gnu/CMakeFiles/CMakeScratch/TryCompile-NT3cvU/src.c:3:
        /usr/include/dirent.h:259:27: note: expected ‘int (*)(const struct dirent *)’ but argument is of type ‘int (*)(struct dirent *)’
          259 |                     int (*__selector) (const struct dirent *),
              |                     ~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        gmake[1]: *** [CMakeFiles/cmTC_d375e.dir/build.make:78: CMakeFiles/cmTC_d375e.dir/src.c.o] Error 1
        gmake[1]: Leaving directory '/builddir/build/BUILD/openbabel-openbabel-3-1-1/x86_64-redhat-linux-gnu/CMakeFiles/CMakeScratch/TryCompile-NT3cvU'
        gmake: *** [Makefile:127: cmTC_d375e/fast] Error 2
        
      exitCode: 2
```

# munge

The package has a failing `getgrent_r` probe:

```
configure:16374: checking for getgrent_r (AIX)
configure:16405: gcc -o conftest -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer   -Wl,-z,relro -Wl,--as-needed  -Wl,-z,now -specs=/usr/lib/rpm/redhat/redhat-hardened-ld -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -Wl,--build-id=sha1 -specs=/usr/lib/rpm/redhat/redhat-package-notes  conftest.c  >&5
conftest.c: In function 'main':
conftest.c:126:48: error: passing argument 4 of 'getgrent_r' from incompatible pointer type
  126 | rv = getgrent_r (&gr, gr_buf, sizeof (gr_buf), &gr_fp);
      |                                                ^~~~~~
      |                                                |
      |                                                FILE **
In file included from conftest.c:115:
/usr/include/grp.h:133:50: note: expected 'struct group ** restrict' but argument is of type 'FILE **'
  133 |                        struct group **__restrict __result)
      |                        ~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~~~~
conftest.c:122:5: warning: variable 'rv' set but not used [-Wunused-but-set-variable]
  122 | int rv;
      |     ^~
configure:16405: $? = 1
```

The AIX probe is expected to fail, but the GNU probe also fails
because of the warning about `rv` (any standard error output causes
the probe to fail).  This is not a GCC 14 regression, it already has
been fixed upstream.

# ncftp, tin, lrzsz, vile, ncurses

This probe:

```
|       int main (void) {
|         /* This call has the arguments reversed.
|            A reversed system may check and see that the address of main
|            is not _IOLBF, _IONBF, or _IOFBF, and return nonzero.  */
|         if (setvbuf(stdout, _IOLBF, (char *) main, BUFSIZ) != 0)
|           exit(1);
|         putc('\r', stdout);
|         exit(0);                      /* Non-reversed systems segv here.  */
|       }
```

Fails with:

```
conftest.c: In function 'main':
conftest.c:149:31: error: passing argument 2 of 'setvbuf' makes pointer from in
teger without a cast
  149 |           if (setvbuf(stdout, _IOLBF, (char *) main, BUFSIZ) != 0)
      |                               ^~~~~~
      |                               |
      |                               int
In file included from conftest.c:140:
/usr/include/stdio.h:339:65: note: expected 'char * restrict' but argument is o
f type 'int'
  339 | extern int setvbuf (FILE *__restrict __stream, char *__restrict __buf,
      |                                                ~~~~~~~~~~~~~~~~~^~~~~
conftest.c:149:39: error: passing argument 3 of 'setvbuf' makes integer from po
inter without a cast
  149 |           if (setvbuf(stdout, _IOLBF, (char *) main, BUFSIZ) != 0)
      |                                       ^~~~~~~~~~~~~
      |                                       |
      |                                       char *
/usr/include/stdio.h:340:25: note: expected 'int' but argument is of type 'char
 *'
  340 |                     int __modes, size_t __n) __THROW __nonnull ((1));
      |                     ~~~~^~~~~~~
```

This is harmless because this is a run-time probe, and it (usually?)
fails at run time if compilation succeeds (!).

# nrpe, nsca

The configure probe for `SOCKET_SIZE_TYPE` does not work:

```
AC_MSG_CHECKING(for type of socket size)
AC_TRY_COMPILE([#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
],
[int a = send(1, (const void *)0, (size_t *) 0, (int *) 0);],
[AC_DEFINE(SOCKET_SIZE_TYPE, size_t, [Socket Size Type]) AC_MSG_RESULT(size_t)],
[AC_DEFINE(SOCKET_SIZE_TYPE, int, [Socket Size Type]) AC_MSG_RESULT(int)])
```

With GCC 13, we pick `size_t` here, but with GCC 14, it will be `int`.
Neither type is correct (it should be `socklen_t`).  However, this
does not matter because `SOCKET_SIZE_TYPE` is not used during the rest
of the build.

# opal

A harmless error (due to the existing `-Werror`) is logged for this
checked:

```
dnl OPAL_SPEEX_FLOAT
dnl Determine whether to use the system or internal speex dsp lib (can be forced), uses pkg-config
dnl Arguments: $SPEEXDSP_CFLAGS
dnl            $SPEEXDSP_LIBS
dnl Return:    $SPEEXDSP_SYSTEM whether system or interal speex dsp lib shall be used
AC_DEFUN([OPAL_SPEEX_FLOAT],
         [
          old_CFLAGS="$CFLAGS"
          old_LIBS="$LIBS"
          CFLAGS="$CFLAGS $SPEEXDSP_CFLAGS -Werror"
          LIBS="$LIBS $SPEEXDSP_LIBS"
          AC_CHECK_HEADERS([speex/speex.h], [speex_inc_dir="speex/"], [speex_inc_dir=])
          AC_LINK_IFELSE([AC_LANG_PROGRAM([[
                          #include <${speex_inc_dir}speex.h>
                          #include <${speex_inc_dir}speex_preprocess.h>
                          #include <stdio.h>]],
                          [[
                            SpeexPreprocessState *st;
                            spx_int16_t *x;
                            float *echo;
                            speex_preprocess(st, x, echo);
                            return 0;
                          ]])], [opal_speexdsp_float=yes], [opal_speexdsp_float=no])
          CFLAGS="$old_CFLAGS"
          LIBS="$old_LIBS"
          OPAL_MSG_CHECK([Speex has float], [$opal_speexdsp_float])

          AS_IF([test AS_VAR_GET([opal_speexdsp_float]) = yes], [$1], [$2])[]
         ])
```

```
conftest.c: In function 'main':
conftest.c:56:53: error: passing argument 3 of 'speex_preprocess' from incompatible pointer type
   56 |                             speex_preprocess(st, x, echo);
      |                                                     ^~~~
      |                                                     |
      |                                                     float *
In file included from conftest.c:47:
/usr/include/speex/speex_preprocess.h:80:77: note: expected 'spx_int32_t *' {aka 'int *'} but argument is of type 'float *'
   80 | int speex_preprocess(SpeexPreprocessState *st, spx_int16_t *x, spx_int32_t *echo);
      |                                                                ~~~~~~~~~~~~~^~~~
```

# perl

A probe related to `va_copy` fails with an incompatible-pointer-types
error:

```
try.c: In function ‘myvfprintf’:
try.c:15:28: warning: passing argument 3 of ‘ivfprintf’ from incompatible pointer type [-Wincompatible-pointer-types]
   15 |   return ivfprintf(f, fmt, &val);
      |                            ^~~~
      |                            |
      |                            __va_list_tag **
try.c:7:46: note: expected ‘__va_list_tag (*)[1]’ but argument is of type ‘__va_list_tag **’
    7 | ivfprintf(FILE *f, const char *fmt, va_list *valp)
      |                                     ~~~~~~~~~^~~~
```

Source code:

```
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int
ivfprintf(FILE *f, const char *fmt, va_list *valp)
{
  return vfprintf(f, fmt, *valp);
}

int
myvfprintf(FILE *f, const  char *fmt, va_list val)
{
  return ivfprintf(f, fmt, &val);
}

int
myprintf(char *fmt, ...)
{
  va_list val;
  va_start(val, fmt);
  return myvfprintf(stdout, fmt, val);
}

int
main(int ac, char **av)
{
  signal(SIGSEGV, exit);
#ifdef SIGBUS
  signal(SIGBUS,  exit);
#endif

  myprintf("%s%cs all right, then\n", "that", '\'');
  exit(0);
}
```

The reason is the peculiar definition of `va_list`.

This is not a problem because the probe fails later on at run time
with the uninstrumented compiler.

# php-pecl-http

There is a glitch in an autoconf probe, a syntax error around
`"SRP""`.  Reported upstream:

* [Glitch in CURL_VERSION_TLSAUTH_SRP autoconf probe](https://github.com/m6w6/ext-http/issues/133)

# pure-ftpd

This is a `sendfile` probe this is supposed to fail:

```
conftest.c: In function 'main':
conftest.c:127:26: error: passing argument 3 of 'sendfile' makes pointer from integer without a cast
  127 |  (void) sendfile(fd, fd, off, cnt, hdtr, &off, 42);
      |                          ^~~
      |                          |
      |                          off_t {aka long int}
In file included from conftest.c:114:
/usr/include/sys/sendfile.h:33:60: note: expected 'off_t *' {aka 'long int *'} but argument is of type 'off_t' {aka 'long int'}
   33 | extern ssize_t sendfile (int __out_fd, int __in_fd, off_t *__offset,
      |                                                     ~~~~~~~^~~~~~~~
conftest.c:127:9: error: too many arguments to function 'sendfile'
  127 |  (void) sendfile(fd, fd, off, cnt, hdtr, &off, 42);
      |         ^~~~~~~~
/usr/include/sys/sendfile.h:33:16: note: declared here
   33 | extern ssize_t sendfile (int __out_fd, int __in_fd, off_t *__offset,
      |                ^~~~~~~~
```

Note the *too many arguments to function* error, which also happens
with a non-instrumented GCC.  A future instrumented GCC update will
not log the int-conversion error in this case.

# python-drgn

An implicit-int error is the result of a legitimate test for support
of the `auto` keyword:

```
conftest.c:23:6: error: type defaults to 'int' in declaration of 'x'
   23 | auto x = 1;
      |      ^
conftest.c:23:6: error: file-scope declaration of 'x' specifies 'auto'
```

# R-XML

This configure probe error does not matter because the probing program
is built with `-pedantic-errors`:

```
conftest.c: In function 'main':
conftest.c:25:22: error: passing argument 2 of 'xmlHashScan' from incompatible pointer type
   25 |   xmlHashScan(table, foo, NULL);
      |                      ^~~
      |                      |
      |                      void * (*)(void *, void *, xmlChar *) {aka void * (*)(void *, void *, unsigned char *)}
In file included from /usr/include/libxml2/libxml/parser.h:16,
                 from /usr/include/libxml2/libxml/SAX.h:16,
                 from conftest.c:15:
/usr/include/libxml2/libxml/hash.h:209:57: note: expected 'xmlHashScanner' {aka 'void (*)(void *, void *, const unsigned char *)'} but argument is of type 'void * (*)(void *, void *, xmlChar *)' {aka 'void * (*)(void *, void *, unsigned char *)'}
  209 |                                          xmlHashScanner scan,
      |                                          ~~~~~~~~~~~~~~~^~~~
```

# rlwrap

A check for the `tputs` function fails like this:

```
conftest.c: In function 'main':
conftest.c:85:15: error: passing argument 3 of 'tputs' from incompatible pointer
 type
   85 | tputs("a", 1, f);
      |               ^
      |               |
      |               int (*)(char)
In file included from conftest.c:70:
/usr/include/term.h:848:54: note: expected 'int (*)(int)' but argument is of type 'int (*)(char)'
  848 | extern NCURSES_EXPORT(int) tputs (const char *, int, int (*)(int));
      |                                                      ^~~~~~~~~~~~
conftest.c: In function 'f':
conftest.c:81:32: warning: control reaches end of non-void function [-Wreturn-type]
   81 |                int f(char i) { }
      |                                ^
```

However, the configure script treats any standard error output as a
test failure and ignores the compiler exit status, so the overal test
outcome does not change due to the new compiler error.


# rtpproxy

The check for `-Wincompatible-pointer-types` fails because it results
in a compiler error:

```
configure:13044: checking whether 'gcc' supports -Wincompatible-pointer-types
configure:13059: gcc -c -pipe -Wincompatible-pointer-types -D_DEFAULT_SOURCE -D
_ISOC99_SOURCE -DLINUX_XXX conftest.c >&5
conftest.c: In function 'main':
conftest.c:29:33: error: assignment to 'struct foo *' from incompatible pointer type 'struct bar *'
   29 | struct foo *f; struct bar *b; f = b;
      |                                 ^
configure:13059: $? = 1
configure: failed program was:
| /* confdefs.h */
[…]
| int
| main (void)
| {
| struct foo *f; struct bar *b; f = b;
|   ;
|   return 0;
| }
configure:13071: result: no
```

This should be harmless because the warning/error is enabled by
default anyway.

Also reported upstream:

* [-Wincompatible-pointer-types configure check may fail unconditionally with future compilers](https://github.com/sippy/rtpproxy/issues/149)

# rubygem-ruby-libvirt

Some `have_const` probes fail because they try to probe for constants
which are actually string literals, and the probing code expects
integers:

```
conftest.c:8:16: warning: initialization of ‘int’ from ‘char *’ makes integer from pointer without a cast [-Wint-conversion]
    8 | static int t = VIR_NODE_MEMORY_SHARED_MERGE_ACROSS_NODES;
      |                ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In file included from /usr/include/libvirt/libvirt.h:35,
                 from conftest.c:5:
conftest.c:8:16: error: initializer element is not computable at load time
    8 | static int t = VIR_NODE_MEMORY_SHARED_MERGE_ACROSS_NODES;
      |                ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
conftest.c:8:12: warning: ‘t’ defined but not used [-Wunused-variable]
    8 | static int t = VIR_NODE_MEMORY_SHARED_MERGE_ACROSS_NODES;
      |            ^
checked program was:
/* begin */
1: #include "ruby.h"
2: 
3: #include "ruby.h"
4: 
5: #include <libvirt/libvirt.h>
6: 
7: /*top*/
8: static int t = VIR_NODE_MEMORY_SHARED_MERGE_ACROSS_NODES;
/* end */
```

This is not a problem because even older GCC refuses to create a
pointer-truncating relocation, so the probe already fails
(incorrectly).  There is an upstream MR to fix this:

* [Remove all the HAVE_ feature conditionals and checks](https://gitlab.com/libvirt/libvirt-ruby/-/merge_requests/28)

# samba

The check “whether `krb5_enctype_to_string` takes `krb5_context`
argument” produces errors:

```
err: ../../test.c: In function ‘main’:
../../test.c:643:31: error: passing argument 1 of ‘krb5_enctype_to_string’ makes integer from pointer without a cast
  643 |        krb5_enctype_to_string(context, 1, &str);
      |                               ^~~~~~~
      |                               |
      |                               krb5_context {aka struct _krb5_context *}
In file included from /usr/include/krb5.h:8,
                 from ../../test.c:637:
/usr/include/krb5/krb5.h:6350:37: note: expected ‘krb5_enctype’ {aka ‘int’} but argument is of type ‘krb5_context’ {aka ‘struct _krb5_context *’}
 6350 | krb5_enctype_to_string(krb5_enctype enctype, char *buffer, size_t buflen);
      |                        ~~~~~~~~~~~~~^~~~~~~
../../test.c:643:40: error: passing argument 2 of ‘krb5_enctype_to_string’ makes pointer from integer without a cast
  643 |        krb5_enctype_to_string(context, 1, &str);
      |                                        ^
      |                                        |
      |                                        int
/usr/include/krb5/krb5.h:6350:52: note: expected ‘char *’ but argument is of type ‘int’
 6350 | krb5_enctype_to_string(krb5_enctype enctype, char *buffer, size_t buflen);
      |                                              ~~~~~~^~~~~~
../../test.c:643:43: error: passing argument 3 of ‘krb5_enctype_to_string’ makes integer from pointer without a cast
  643 |        krb5_enctype_to_string(context, 1, &str);
      |                                           ^~~~
      |                                           |
      |                                           char **
/usr/include/krb5/krb5.h:6350:67: note: expected ‘size_t’ {aka ‘long unsigned int’} but argument is of type ‘char **’
 6350 | krb5_enctype_to_string(krb5_enctype enctype, char *buffer, size_t buflen);
      |                                                            ~~~~~~~^~~~~~
```

This is not a problem because the test is compiled with `-Werror`
unconditionally.

# scalasca

A configure probe fails due to an unknown type and an implicit
function declaration:

```
conftest.c:63:40: error: unknown type name 'off64_t'; did you mean 'off_t'?
   63 |             int fseeko64(FILE *stream, off64_t offset, int whence);
      |                                        ^~~~~~~
      |                                        off_t
conftest.c: In function 'main':
conftest.c:69:24: error: implicit declaration of function 'fseeko64'
   69 |                 return fseeko64(stream, 256lu, 0);
      |                        ^~~~~~~~
```

This is probably a bug (we have `fseeko64`), but it's not a change due
to the preceding type error.  GCC instrumentation could be enhanced to
suppress reporting in this case.

# shellinabox

A probe fails with the message below, but it does not matter because
the configure script uses `AC_LANG_WERROR` (which is misnamed because
it does *not* actually use `-Werror`):

```
In file included from /usr/include/grp.h:25,
                 from conftest.c:50:
/usr/include/features.h:196:3: warning: #warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE" [-Wcpp]
  196 | # warning "_BSD_SOURCE and _SVID_SOURCE are deprecated, use _DEFAULT_SOURCE"
      |   ^~~~~~~
conftest.c: In function 'main':
conftest.c:55:45: error: initialization of 'int (*)(const char *, int,  int *, int *)' from incompatible pointer type 'int (*)(const char *, __gid_t,  __gid_t *, int *)' {aka 'int (*)(const char *, unsigned int,  unsigned int *, int *)'}
   55 | int (*f)(const char *, int, int *, int *) = getgrouplist;
      |                                             ^~~~~~~~~~~~
conftest.c:55:7: warning: unused variable 'f' [-Wunused-variable]
   55 | int (*f)(const char *, int, int *, int *) = getgrouplist;
      |       ^
```

# skopeo

Lots of errors like this:

```
<stdin>:4: implicit-function-declaration: get_subuid_ranges
<stdin>:5: implicit-function-declaration: free
<stdin>:4: implicit-function-declaration: get_subuid_ranges
<stdin>:5: implicit-function-declaration: free
<stdin>:5: implicit-function-declaration: free
<stdin>:4: implicit-function-declaration: get_subuid_ranges
<stdin>:5: implicit-function-declaration: free
```

It seem sthey stem from `hack/libsubid_tag.sh`, but this probe also
fails with older GCC, so it is a false positive.  This is probably not
what is intended, and a bug was filed:

* [skopeo: Build tags apparently not set correctly](https://bugzilla.redhat.com/show_bug.cgi?id=2254902)x

# suricata

An incompatible-pointer-types probe fails, but `-Werror` or
`-Werror=implicit-function-declaration` is used:

```
configure:21092: checking for signed nfq_get_payload payload argument
configure:21120: gcc -c -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecor
d-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-dec
laration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_G
LIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protect
or-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -
fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-f
rame-pointer -mno-omit-leaf-frame-pointer  -std=c11 -Werror  -I/usr/include/hs  
conftest.c >&5
conftest.c: In function 'main':
conftest.c:171:43: error: passing argument 2 of 'nfq_get_payload' from incompatible pointer type
  171 |                     nfq_get_payload(NULL, &pktdata);
      |                                           ^~~~~~~~
      |                                           |
      |                                           char **
In file included from conftest.c:164:
/usr/include/libnetfilter_queue/libnetfilter_queue.h:123:67: note: expected 'unsigned char **' but argument is of type 'char **'
  123 | extern int nfq_get_payload(struct nfq_data *nfad, unsigned char **data);
      |                                                   ~~~~~~~~~~~~~~~~^~~~
configure:21120: $? = 1
```

# weechat, fcitx, fcitx-cloudpinyin, gammu, libwbxml

This is a check whether an argument to `iconv` is of type `char **` or
`const char **`, and the incompatible-pointer-types error is expected.
(The test is built with `-Werror`.)

```
      variable: "ICONV_2ARG_IS_CONST"
      cached: true
      stdout: |
        Change Dir: '…/redhat-linux-build/CMakeFiles/CMakeScratch/TryCompile-nXLDFB'
        
        Run Build Command(s): /usr/bin/cmake -E env VERBOSE=1 /usr/bin/gmake -f Makefile cmTC_b92e1/fast
        /usr/bin/gmake  -f CMakeFiles/cmTC_b92e1.dir/build.make CMakeFiles/cmTC_b92e1.dir/build
        gmake[1]: Entering directory '…/redhat-linux-build/CMakeFiles/CMakeScratch/TryCompile-nXLDFB'
        Building C object CMakeFiles/cmTC_b92e1.dir/src.c.o
        /usr/bin/gcc -DICONV_2ARG_IS_CONST  -O2 -flto=auto -ffat-lto-objects -fexceptions -g -grecord-gcc-switches -pipe -Wall -Werror=format-security -Werror=implicit-function-declaration -Werror=implicit-int -Wp,-U_FORTIFY_SOURCE,-D_FORTIFY_SOURCE=3 -Wp,-D_GLIBCXX_ASSERTIONS -specs=/usr/lib/rpm/redhat/redhat-hardened-cc1 -fstack-protector-strong -specs=/usr/lib/rpm/redhat/redhat-annobin-cc1  -m64   -mtune=generic -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer -fsigned-char -fms-extensions -Wall -Wextra -Werror-implicit-function-declaration -Wformat -Werror=format-security -Werror -o CMakeFiles/cmTC_b92e1.dir/src.c.o -c …/redhat-linux-build/CMakeFiles/CMakeScratch/TryCompile-nXLDFB/src.c
        …/redhat-linux-build/CMakeFiles/CMakeScratch/TryCompile-nXLDFB/src.c: In function ‘main’:
        …/redhat-linux-build/CMakeFiles/CMakeScratch/TryCompile-nXLDFB/src.c:9:17: error: passing argument 2 of ‘iconv’ from incompatible pointer type
            9 |     iconv(conv, &in, &ilen, &out, &olen);
              |                 ^~~
              |                 |
              |                 const char **
        In file included from …/redhat-linux-build/CMakeFiles/CMakeScratch/TryCompile-nXLDFB/src.c:2:
        /usr/include/iconv.h:49:54: note: expected ‘char ** restrict’ but argument is of type ‘const char **’
           49 | extern size_t iconv (iconv_t __cd, char **__restrict __inbuf,
              |                                    ~~~~~~~~~~~~~~~~~~^~~~~~~
        gmake[1]: *** [CMakeFiles/cmTC_b92e1.dir/build.make:78: CMakeFiles/cmTC_b92e1.dir/src.c.o] Error 1
```

The `fcitx` and `fcitx-cloudpinyin` packages have the same issue, for
the LIBICONV_SECOND_ARGUMENT_IS_CONST check.  For `libwbxml`, it is
called `ICONV_SECOND_ARGUMENT_IS_CONST`.

# xen, disk-utilities

The way compiler option probing is done in `mini-os` triggers
int-conversion errors:

```
cc-option = $(shell if test -z "`echo 'void*p=1;' | \
              $(1) $(2) -S -o /dev/null -x c - 2>&1 | grep -- $(2) -`"; \
              then echo "$(2)"; else echo "$(3)"; fi ;)
```

But the shell script ignores the compiler exit status, so that does
not matter.

For disk-utilities, this check resides in `Rules.mk`.

# xmms2

A Ruby-related probe:

```
Checking for header ruby.h
==>

    #include <ruby.h>
    int main(void) {
        rb_protect_inspect(0,0,0);
        return 0;
    }
```

Fails with:

```
err: ../test.c: In function ‘main’:
../test.c:4:9: error: implicit declaration of function ‘rb_protect_inspect’; did you mean ‘rb_str_inspect’? [-Werror=implicit-function-declaration]
    4 |         rb_protect_inspect(0,0,0);
      |         ^~~~~~~~~~~~~~~~~~
      |         rb_str_inspect
cc1: some warnings being treated as errors
```

But this is not a problem because `-Werror=implicit-function-declaration`
is always used.
