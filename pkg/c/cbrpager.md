* [-Wreturn-mismatch C compatibility fix](https://sourceforge.net/p/cbrpager/support-requests/5/)
* https://src.fedoraproject.org/rpms/cbrpager/c/c3eabdf4042b4c85725b0f88a3dd67e1b8b7e905?branch=rawhide (-Wreturn-mismatch)
