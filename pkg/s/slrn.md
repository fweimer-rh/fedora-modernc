* [autoconf: Fix C99 compatibility issue in the VA_COPY checks](https://github.com/jedsoft/slrn/pull/1)
* https://src.fedoraproject.org/rpms/slrn/c/c75ff0a4111fddf292a7476b400da20fa07535db?branch=rawhide
