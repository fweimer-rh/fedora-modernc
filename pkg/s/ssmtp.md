* https://src.fedoraproject.org/rpms/ssmtp/c/8f09cf50dda123a17c005a63aafe5b7e5f6ac41d?branch=rawhide
* [ssmtp: GCC 14 compatibility fixes](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1060065)
* https://src.fedoraproject.org/rpms/ssmtp/c/4fc43d8de3609592ee0d980ed2a55aeb28f89a53?branch=rawhide
