* [Lua plugin: Add pointer casts for C89 compatibility](https://github.com/libpinyin/ibus-libpinyin/pull/449)
* https://src.fedoraproject.org/rpms/ibus-libpinyin/c/651d366bca28d22638bfd6575b8c1abdae88ce80?branch=rawhide
