* [Fix C99 compatibility issue](https://github.com/mistydemeo/ebnetd/pull/1)
* https://src.fedoraproject.org/rpms/ebnetd/c/65be0d97991118050e8bd5073d18f2e52bdd8558?branch=rawhide
