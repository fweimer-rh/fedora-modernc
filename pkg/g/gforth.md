* [configure: C99 compatibility fix for -export-dynamic, assembler skip checks](https://savannah.gnu.org/bugs/?64068)
* https://src.fedoraproject.org/rpms/gforth/c/5de29dac85abcd72b581aaaaf92d8f761087f537?branch=rawhide
