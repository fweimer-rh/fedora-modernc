* [sunmenu: Define _GNU_SOURCE, so that <time.h> declares strptime](https://github.com/XEphem/XEphem/pull/73)
* https://src.fedoraproject.org/rpms/xephem/c/6098e65f257349c14bfc24ade87edaac8b147874?branch=rawhide
