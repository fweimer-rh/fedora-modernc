* [rubygem-xmlparser: broken because it calls undefined ENC_TO_ENCINDEX function](https://bugzilla.redhat.com/show_bug.cgi?id=2185007)
* https://src.fedoraproject.org/rpms/rubygem-xmlparser/c/60a73a2f2c3bdefe950233bedae8e7a493515b5d?branch=rawhide
* https://src.fedoraproject.org/rpms/rubygem-xmlparser/c/92ca5216b0b65ef15a3f72cced7610489d3a0ee6?branch=rawhide
* https://bugzilla.redhat.com/show_bug.cgi?id=2256626
* https://src.fedoraproject.org/rpms/rubygem-xmlparser/c/062bd3cfcc8f3389eede98f47a4b99b300f4f800?branch=rawhide
