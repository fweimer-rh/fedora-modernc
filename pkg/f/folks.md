* [folks: Build in C89 mode due to Vala compiler limitations](https://bugzilla.redhat.com/show_bug.cgi?id=2159284)
* [C99 compatibility of internal setters](https://discourse.gnome.org/t/c99-compatibility-of-internal-setters/13360)
* [valac does not respect internal header/vapi setting](https://gitlab.gnome.org/GNOME/vala/-/issues/358)
* https://src.fedoraproject.org/rpms/folks/c/9e1de7c4e66d731e12dd93db8436c945603fcf24?branch=rawhide
* [codegen: Emit GCC diagnostics pragmata for GCC 14 compatibility](https://gitlab.gnome.org/GNOME/vala/-/merge_requests/369)
* [Downgrade GCC 14 C type errors to warnings](https://src.fedoraproject.org/rpms/vala/c/6c9c11e678f5c693c437fa73ccb5cb0c2d33fc84?branch=rawhide)
