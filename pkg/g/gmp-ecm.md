* [fix for clang 12 from Apple's Xcode 12](https://gitlab.inria.fr/zimmerma/ecm/-/commit/8fbb2809afe72799098e07d7c74fef4d6febbf70)
* https://src.fedoraproject.org/rpms/gmp-ecm/c/256c654e0823edfd68e74dbdfb5ac9820fff54ed?branch=rawhide
