* [Make configure vsnprintf() check more comprehensive](https://github.com/dajobe/raptor/commit/a7da6f35813e30124765d995d9053cb7f8529ca4)
* https://src.fedoraproject.org/rpms/raptor/c/ca96b703fa17fb10016673d06c251274bb635af4?branch=rawhide
