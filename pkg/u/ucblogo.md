* [Avoid more implicit function declarations](https://github.com/jrincayc/ucblogo-code/pull/156)
* https://src.fedoraproject.org/rpms/ucblogo/c/dc47d79b012178c08778fa044850901d4a4526fe?branch=rawhide
* [Define SIG_TAKES_ARG in unix.](https://github.com/jrincayc/ucblogo-code/commit/30b1223b4979a18132d599792c995264c299ba4e)
* [Fixing incorrect return and wrong type warnings.](https://github.com/jrincayc/ucblogo-code/commit/d300824c6eaf5f98faded7c51ea799b32be7a50b)
* https://src.fedoraproject.org/rpms/ucblogo/c/5cceaf29a22076edd21e21e5a0308252042a4844?branch=rawhide
