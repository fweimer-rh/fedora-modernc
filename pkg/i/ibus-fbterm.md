* [configure.ac: Use AC_USE_SYSTEM_EXTENSIONS](https://github.com/fujiwarat/ibus-fbterm/pull/1)
* https://src.fedoraproject.org/rpms/ibus-fbterm/c/30911cd248847d56b6e819aea248587ee17227e3?branch=rawhide
* [fb_io_get_fd returns a value and needs to use g_return_val_if_fail](https://github.com/fujiwarat/ibus-fbterm/pull/2)
* [codegen: Emit GCC diagnostics pragmata for GCC 14 compatibility](https://gitlab.gnome.org/GNOME/vala/-/merge_requests/369)
* [Downgrade GCC 14 C type errors to warnings](https://src.fedoraproject.org/rpms/vala/c/6c9c11e678f5c693c437fa73ccb5cb0c2d33fc84?branch=rawhide)
* [Downgrade GCC 14 -Wint-conversion errors to warnings](https://src.fedoraproject.org/rpms/vala/c/67fbbfc5e7b859ad8081c8332f982c83ceec922a?branch=rawhide)
* https://src.fedoraproject.org/rpms/ibus-fbterm/c/9652750a2ed204e28a719c1a2347f0878e8d26a0?branch=rawhide
