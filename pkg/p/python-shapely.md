* [Fix incompatible pointer type passed to GEOSPolygonize_r](https://github.com/shapely/shapely/pull/1945)
* https://src.fedoraproject.org/rpms/python-shapely/c/964cd9531df18e8f8aeb07164f9ac09a68eeeab2?branch=rawhide
