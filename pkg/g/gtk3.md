* [tests: Add GdkEvent casts in testinput](https://gitlab.gnome.org/GNOME/gtk/-/commit/76bc2a57136fd6cf0374fa3f86a7ba646b779803)
* [testsuite: Fix casts in reftests](https://gitlab.gnome.org/GNOME/gtk/-/commit/05a12b1c5f8eecc621a7135eb313d6bfe6ccdc3b)
* https://src.fedoraproject.org/rpms/gtk3/c/3f8b4630a020bc37be1849d74fc17ad25f323d93?branch=rawhide
