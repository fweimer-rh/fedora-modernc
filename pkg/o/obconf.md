* [C99 compatibility fixes (implicit function declarations)](https://bugzilla.icculus.org/show_bug.cgi?id=6671)
* https://src.fedoraproject.org/rpms/obconf/c/ddbecac8f34a14b06da4797754216b9c51184e2d?branch=rawhide
