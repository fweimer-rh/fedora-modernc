* [Fix build: use correct variable for category and env](https://github.com/rpm-software-management/libcomps/commit/a71bce7e62990550a57688e51b14eb82d6de196b)
* https://src.fedoraproject.org/rpms/libcomps/c/32d6dd5c605d4553c30d8ddac80be80ddd4760ce?branch=rawhide
