* [vinagre: Add missing type to dontoptimiseaway definitions (for C99 compatibility)](https://bugzilla.redhat.com/show_bug.cgi?id=2179869)
* https://src.fedoraproject.org/rpms/vinagre/c/1e1f2893dfb94097695a2a3a24d94c3c4eb255a0?branch=rawhide
