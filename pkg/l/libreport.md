* [Fix compatibility with libxml2 2.12](https://github.com/abrt/libreport/pull/801)
* https://src.fedoraproject.org/rpms/libreport/c/3d476b7c77a27fee1f9cc335ac011346964b744d?branch=rawhide
* [Fix pointer type compatibility issues in tests](https://github.com/abrt/libreport/pull/802)
* https://src.fedoraproject.org/rpms/libreport/c/3265c40aad6548917e7f6de898563d71d17afc0c?branch=rawhide
