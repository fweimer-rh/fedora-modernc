* [configure: Fix type errors in __thread test](https://github.com/mono/mono/pull/21730)
* https://src.fedoraproject.org/rpms/wine-mono/c/474ef6aeffa4e49da779b4a8fe43a700c999b35f?branch=rawhide
