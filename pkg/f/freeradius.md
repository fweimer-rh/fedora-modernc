* [configure: Fix type of argument to backtrace_symbols](https://github.com/FreeRADIUS/freeradius-server/pull/5246)
* https://src.fedoraproject.org/rpms/freeradius/c/1793f410aa789704b5ac0be9cf7d0eaece906d1a?branch=rawhide
