* [8307542: Call to FcConfigAppFontAddFile uses wrong prototype, arguments](https://github.com/openjdk/jfx/pull/1128)
* https://bugs.openjdk.org/browse/JDK-8307542
* https://src.fedoraproject.org/rpms/openjfx8/c/f8344adba5f198ce481d8766593eba28bed6f1b8?branch=rawhide
