* [configure.ac: Avoid implicit int in libbz2 probe](https://gitlab.gnome.org/GNOME/libgsf/-/merge_requests/13)
* https://src.fedoraproject.org/rpms/libgsf/c/f7839542bf4d1fb9c0a4681063680f2269fa42ce?branch=rawhide
