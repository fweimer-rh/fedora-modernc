* [Fix #583: python 3.12 compatibility (#598)](https://github.com/urwid/urwid/commit/44f46f9f389816c154d80a7d3ceddb3730e6eb88)
* https://src.fedoraproject.org/rpms/python-urwid/c/e871fbf36518b0700ce26654d06db002877aee19?branch=rawhide
