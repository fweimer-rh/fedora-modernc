* [Fix various prototype inconsistencies.](https://github.com/xiaoyeli/superlu_mt/commit/7d2f3855060a6043bc4953b6139e6ad6a593e08c)
* [Fix C99 incompatibilities in examples](https://github.com/xiaoyeli/superlu_mt/pull/6)
* [https://github.com/xiaoyeli/superlu_mt/pull/7](https://github.com/xiaoyeli/superlu_mt/pull/7)
* https://src.fedoraproject.org/rpms/SuperLUMT/c/5bf370bc9f1f366f1910fb789a38c95e6f8790fc?branch=rawhide
