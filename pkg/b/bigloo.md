* [Migrate K&R C in the config scripts to ANSI C](https://github.com/manuel-serrano/bigloo/commit/85aed0c60431104446bb904d44066be0bae225d1)
* [New bmem memory profiler](https://github.com/manuel-serrano/bigloo/commit/d315487d6a97ef7b4483e919d1823a408337bd07)
* https://src.fedoraproject.org/rpms/bigloo/c/71da92d4f750b38420e929e4cafd98436ccf2a1c?branch=rawhide
* [Prepare for GCC 14](https://github.com/manuel-serrano/bigloo/pull/104)
* https://src.fedoraproject.org/rpms/bigloo/c/a877f9a70d648f12b6433f6f94ad2389faf90b40?branch=rawhide
