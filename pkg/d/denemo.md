* [Fix C99 support in sffile.c](https://savannah.gnu.org/bugs/index.php?63720)
* https://src.fedoraproject.org/rpms/denemo/c/fff401e8589d540ce546e55518f635f64d1bc3e6?branch=rawhide
