* [Code cleanup, such as K&R-c to ANSI-C. Adapted from Christian Werner's Androwish work](https://sourceforge.net/p/tkimg/code/459/)
* https://src.fedoraproject.org/rpms/tkimg/c/571ed2643d3a802a53af146360ecbbe1dfb01048?branch=rawhide
* [Update TIFF extensions (Jpeg, Pixar and Zip) to the latest versions](https://sourceforge.net/p/tkimg/code/587/)
* [Backport part of an upstream patch to fix C compatibility issues](https://src.fedoraproject.org/rpms/tkimg/c/473e655b5938d1954ffbf2963fd2905d56492f4d?branch=rawhide)
* [Fix incorrect patch description](https://src.fedoraproject.org/rpms/tkimg/c/39672326ae455f09e6cf9dfe530951716f000bc5?branch=rawhide)
