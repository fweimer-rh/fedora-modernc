* [Avoid incompatible pointer type error with GCC 14](https://github.com/sympy/sympy/pull/25968)
* https://src.fedoraproject.org/rpms/sympy/c/e6b3f6e09470c082f0ed3c0589905ccd09798a82?branch=rawhide
