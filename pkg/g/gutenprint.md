* [gimpui:  Fix two implicit-pointer-cast-to-int instances](https://sourceforge.net/p/gimp-print/source/ci/75af7caf4d3369de2d0c5432220da15fae64f58b/)
* https://src.fedoraproject.org/rpms/gutenprint/c/a41e562922bd6c7d6fb7893251bce41802d8cd3c?branch=rawhide
