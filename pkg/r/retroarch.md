* [(Wayland) Silence implicit declaration warnings](https://github.com/libretro/RetroArch/commit/96d39a78ccf3a1aabbea2e68072de2255f2285d3)
* https://src.fedoraproject.org/rpms/retroarch/c/d074b97370594d76389c38b14d6092849f6bccea?branch=rawhide
