* [Define _GNU_SOURCE for a declaration of strptime](https://lore.kernel.org/linux-efi/87fsdhllhk.fsf@oldenburg.str.redhat.com/)
* [efitools: Include <strings.h> for the strcasecmp function](https://lore.kernel.org/linux-efi/87pmcllll9.fsf@oldenburg.str.redhat.com/)
* https://src.fedoraproject.org/rpms/efitools/c/02f9b8d7b94013b92ea43855708925285724f1d7?branch=rawhide
